<?php
/**
 *
 * @package WordPress
 * @subpackage Zebrabus
 * @since 1.0
 * @version 1.0
 */
get_header(); ?>
<section class="page__section">
    <div class="container">
        <div class="row">
            <div class="col">
                <h1 data-aos="fade-left"><?php single_post_title(); ?></h1>
            </div>
        </div>
        <?php 
        $i = 0;
        if ( have_posts() ) : ?>
            <div class="row">
            <?php while ( have_posts() ) : the_post();
                if($i == 3) {
                    get_template_part( 'template-parts/post/blog-banner' );
                    get_template_part( 'template-parts/post/content' );
                } else {
                    get_template_part( 'template-parts/post/content' );
                }
                
            $i++; endwhile; ?>
            </div>
        <?php endif; wp_reset_postdata(); ?>
    </div>
</section>
<?php get_template_part( 'inc/acf-content/subscribe-section' ); 

get_footer();
<?php
/**
 *
 * @package WordPress
 * @subpackage Zebrabus
 * @since 1.0
 */

/*Trainers post type*/
require get_template_directory() . '/inc/post-type-function.php';

/*Translation*/
require get_template_directory() . '/inc/translation.php';

/*ACF Import*/
require get_template_directory() . '/inc/acf-import.php';

/*Custom Widgets*/
require get_template_directory() . '/inc/widgets-custom.php';

/*Theme setup*/
function zebrabus_setup() {
    load_theme_textdomain( 'zebrabus' );
    add_theme_support( 'automatic-feed-links' );
    add_theme_support( 'title-tag' );
    add_theme_support( 'post-thumbnails' );

    add_image_size( 'gallery', 300, 300, true );
    add_image_size( 'gallery-thumbnail', 410, 250, true );
    // add_image_size( 'portfolio-thumbnail', 650, 330, true );
    // add_image_size( 'portfolio-medium', 825, 464, true );

    register_nav_menus( array(
        'main'          => __( 'Main Menu', 'zebrabus' )
    ) );
}
add_action( 'after_setup_theme', 'zebrabus_setup' );

/*App2drive styles and scripts*/
function zebrabus_scripts() {
    $version = '1.0.21';

    wp_enqueue_style( 'zebrabus-css', get_theme_file_uri( '/assets/css/main.min.css' ), '', $version );
    wp_enqueue_style( 'zebrabus-style', get_stylesheet_uri() );

    wp_enqueue_script( 'slick-js', get_theme_file_uri( '/assets/js/slick.min.js' ), array( 'jquery' ), $version, true );
    wp_enqueue_script( 'mousewheel-smoothscroll-js', get_theme_file_uri( '/assets/js/mousewheel-smoothscroll.min.js' ), array( 'jquery' ), $version, true );
    wp_enqueue_script( 'google-maps-key', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyDOqauekT_r4JzPY18FOWN4N8DFR3hXo1U&libraries=places', array( 'jquery' ), $version, true );
    wp_enqueue_script( 'share-js', '//platform-api.sharethis.com/js/sharethis.js#property=5b77cc47f8352a0011896bd5&product=custom-share-buttons', array('jquery'), $version, true );
    wp_enqueue_script( 'lightgallery-js', get_theme_file_uri( '/assets/js/lightgallery.min.js' ), array( 'jquery' ), $version, true );
    wp_enqueue_script( 'aos-js', get_theme_file_uri( '/assets/js/aos.min.js' ), array( 'jquery' ), $version, true );
    wp_enqueue_script( 'select-js', get_theme_file_uri( '/assets/js/select2.min.js' ), array( 'jquery' ), $version, true );
    wp_enqueue_script( 'parallax-js', get_theme_file_uri( '/assets/js/parallax.min.js' ), array( 'jquery' ), $version, true );
    wp_enqueue_script( 'script-js', get_theme_file_uri( '/assets/js/main.min.js' ), array( 'jquery' ), $version, true );
}
add_action( 'wp_enqueue_scripts', 'zebrabus_scripts' );

/*Theme settings*/
if( function_exists('acf_add_options_page') ) {
    $general = acf_add_options_page(array(
        'page_title'    => 'Theme General Settings',
        'menu_title'    => 'Theme Settings',
        'redirect'      => false,
        'capability'    => 'edit_posts',
        'menu_slug'     => 'theme-settings',
    ));
}

/*Register sidebar*/
function zebrabus_widgets_init() {
    register_sidebar( array(
        'name'          => __( 'Footer 1', 'app2drive' ),
        'id'            => 'footer-1',
        'description'   => __( 'Add widgets here to appear in your footer.', 'app2drive' ),
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget'  => '</section>',
        'before_title'  => '<h6>',
        'after_title'   => '</h6>',
    ) );
    register_sidebar( array(
        'name'          => __( 'Footer 2', 'app2drive' ),
        'id'            => 'footer-2',
        'description'   => __( 'Add widgets here to appear in your footer.', 'app2drive' ),
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget'  => '</section>',
        'before_title'  => '<h6>',
        'after_title'   => '</h6>',
    ) );
    register_sidebar( array(
        'name'          => __( 'Footer 3', 'app2drive' ),
        'id'            => 'footer-3',
        'description'   => __( 'Add widgets here to appear in your footer.', 'app2drive' ),
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget'  => '</section>',
        'before_title'  => '<h6>',
        'after_title'   => '</h6>',
    ) );
    register_sidebar( array(
        'name'          => __( 'Footer 4', 'app2drive' ),
        'id'            => 'footer-4',
        'description'   => __( 'Add widgets here to appear in your footer.', 'app2drive' ),
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget'  => '</section>',
        'before_title'  => '<h6>',
        'after_title'   => '</h6>',
    ) );
}
add_action( 'widgets_init', 'zebrabus_widgets_init' );

/*SVG support*/
function zebrabus_svg_types($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter('upload_mimes', 'zebrabus_svg_types');

/*App2drive ajax*/
function zebrabus_ajaxurl() {
?>
    <script type="text/javascript">
        var ajaxurl = '<?php echo admin_url('admin-ajax.php'); ?>';
        <?php if( get_field( 'api_key', 'option' ) ) { ?>
            var apiKey = '<?php the_field( 'api_key', 'option' ); ?>';
        <?php } ?>
    </script>
<?php
}
add_action('wp_footer','zebrabus_ajaxurl');

/*String to slug gunction*/
function zebra_slug($str){
    $str = strtolower(trim($str));
    $str = preg_replace('/[^a-z0-9-]/', '-', $str);
    $str = preg_replace('/-+/', "-", $str);
    return $str;
}

function zebra_no_limit_stops( $query ) {
    if( ! is_admin()
        && $query->is_post_type_archive( 'stops' )
        && $query->is_main_query() ){
            $query->set( 'posts_per_page', -1 );
    }
}
add_action( 'pre_get_posts', 'zebra_no_limit_stops' );
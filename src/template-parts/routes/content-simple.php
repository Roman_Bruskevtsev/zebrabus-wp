<?php if( is_archive() ) { ?>
<div class="col-md-12 col-lg-6">
    <div class="routes__simple" data-aos="fade-up">
        <?php if( get_sub_field('title') ) { ?><h4><?php the_sub_field('title') ?></h4><?php } ?>
        <?php if( get_sub_field('button_link') ) { ?><a href="<?php the_sub_field('button_link'); ?>" class="btn simple__btn"><?php the_sub_field('button_label'); ?></a><?php } ?>
    </div>
</div>
<?php } else { 
?>
<div class="col-md-12 col-lg-6">
    <div class="routes__simple" data-aos="fade-up">
        <?php if( $post['title'] ) { ?><h4><?php echo $post['title']; ?></h4><?php } ?>
        <?php if( $post['button_link'] ) { ?><a href="<?php echo $post['button_link']; ?>" class="btn simple__btn"><?php echo $post['button_label']; ?></a><?php } ?>
    </div>
</div>
<?php } ?> 
<?php 
$background = ( get_the_post_thumbnail( get_the_ID() ) ) ? ' style="background-image: url('.get_the_post_thumbnail_url( get_the_ID(), 'large' ).');"' : '';
?>
<div class="col-md-6 col-lg-6">
    <a href="<?php the_permalink(); ?>" class="routes__post"<?php echo $background; ?>>
        <div class="detail">
            <h4><?php the_title(); ?></h4>
            <?php if( have_rows('stops') ): ?>
            <h6><?php _e('By bus from:', 'zebrabus'); ?></h6>
            <ul class="stops__list">
            <?php while ( have_rows('stops') ) : the_row(); ?>
                <li><?php the_sub_field('stop'); ?></li>
            <?php endwhile; ?>
            </ul>
            <?php endif; ?>
            <span class="details__btn"><?php _e('See details', 'zebrabus'); ?></span>
        </div>
    </a>
</div>
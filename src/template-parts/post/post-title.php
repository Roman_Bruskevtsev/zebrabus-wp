<?php 
$background = ( get_the_post_thumbnail( get_the_ID() ) ) ? ' style="background-image: url('.get_the_post_thumbnail_url( get_the_ID(), 'full' ).');"' : '';
?>
<div class="row">
    <div class="col">
        <div class="post__banner"<?php echo $background; ?> data-aos="fade-up"></div>
    </div>
</div>
<div class="row">
    <div class="col-md-8 col-lg-6">
        <div class="title__block" data-aos="fade-up">
            <h1><?php the_title(); ?></h1>
            <span><?php the_date(); ?></span>
        </div>
    </div>
    <div class="col-md-4 col-lg-6 text-right">
        <div class="share__block" data-aos="fade-up">
            <span><?php _e('Share this article:', 'zebrabus'); ?></span>
            <ul>
                <li>
                    <i class="twitter st-custom-button" data-network="twitter" data-short-url="<?php the_permalink();?>" data-title="<?php the_title(); ?>" data-description="<?php the_excerpt(); ?>" data-image="<?php echo get_the_post_thumbnail_url(); ?>"></i>
                </li>
                <li>
                    <i class="facebook st-custom-button" data-network="facebook" data-short-url="<?php the_permalink();?>" data-title="<?php the_title(); ?>" data-description="<?php the_excerpt(); ?>" data-image="<?php echo get_the_post_thumbnail_url(); ?>"></i>
                </li>
            </ul>
        </div>
    </div>
</div>
<?php 
$background = ( get_the_post_thumbnail( get_the_ID() ) ) ? ' style="background-image: url('.get_the_post_thumbnail_url( get_the_ID(), 'large' ).');"' : '';
?>
<div class="col-lg-4">
    <a href="<?php the_permalink(); ?>" class="article__post">
        <div class="thumbnail"<?php echo $background; ?>></div>
        <div class="detail">
            <span class="data"><?php echo get_the_date(); ?></span>
            <h4><?php the_title(); ?></h4>
        </div>
    </a>
</div>
<?php 
$more_posts = get_sub_field('more_posts_block');
$blog_page = (int) get_option( 'page_for_posts' );
if( $blog_page ){?>
<div class="col-lg-4">
    <div class="more__posts">
        <?php if( $more_posts['title'] ){ ?>
        <h4><?php echo $more_posts['title']; ?></h4>
        <?php } ?>
        <a href="<?php echo get_permalink($blog_page); ?>" class="btn yellow__btn"><?php echo $more_posts['label']; ?></a>
    </div>
</div>
<?php } ?>
<?php 
$blog_id = (int) get_option('page_for_posts');
$banner = get_field('blog_banner', $blog_id);
if($banner) { 
?>
<div class="col-lg-12">
    <div class="blog__banner">
        <div class="content">
            <?php if($banner['title']) { ?>
                <h2 data-aos="fade-up"><?php echo $banner['title']; ?></h2>
            <?php } ?>
            <?php if($banner['button_link']) { ?>
                <a href="<?php echo $banner['button_link']; ?>" class="btn green__btn shadow" data-aos="fade-up"><?php echo $banner['button_label']; ?></a>
            <?php } ?>
        </div>
        <?php 
        $bus = $banner['bus_image'];
        if( $bus ) { ?>
        <div class="small__zebrabus" data-aos="fade-left">
            <?php if($banner['bus_text']) { ?><h2><?php echo $banner['bus_text']; ?></h2><?php } ?>
            <img src="<?php echo $bus['url']; ?>" alt="<?php echo $bus['title']; ?>"> 
        </div>
        <?php } ?>
    </div>
</div>
<?php } ?>
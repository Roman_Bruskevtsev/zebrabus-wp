<?php
/**
 *
 * @package WordPress
 * @subpackage Zebrabus
 * @since 1.0
 * @version 1.0
 */
get_header(); ?>
<section class="single">
    <div class="container">
        <?php
        get_template_part( 'template-parts/post/post-title' );
        if( have_rows('content') ):
            while ( have_rows('content') ) : the_row();
                if( get_row_layout() == 'content_editor' ): 
                    get_template_part( 'inc/acf-content/content-editor' );
                elseif ( get_row_layout() == 'gallery' ): 
                    get_template_part( 'inc/acf-content/gallery' );
                elseif ( get_row_layout() == 'content_banner' ): 
                    get_template_part( 'inc/acf-content/content-banner' );
                endif;
            endwhile;
        endif; ?>
    </div>
</section>
<?php get_template_part( 'inc/acf-content/subscribe-section' ); 
get_footer(); ?>
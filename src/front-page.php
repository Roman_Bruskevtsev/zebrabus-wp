<?php
/**
 *
 * @package WordPress
 * @subpackage Zebrabus
 * @since 1.0
 * @version 1.0
 */
get_header(); 

if( have_rows('content') ):
    while ( have_rows('content') ) : the_row();
        if( get_row_layout() == 'information_banner' ): 
            get_template_part( 'inc/acf-content/information-banner' );
        elseif( get_row_layout() == 'routes_section' ): 
            get_template_part( 'inc/acf-content/routes-section' );
        elseif( get_row_layout() == 'news_section' ): 
            get_template_part( 'inc/acf-content/news-section' );
        elseif( get_row_layout() == 'coach_hire_section' ): 
            get_template_part( 'inc/acf-content/coach-hire-section' );
        elseif( get_row_layout() == 'our_stops_sections' ): 
            get_template_part( 'inc/acf-content/our-stops-section' );
        elseif( get_row_layout() == 'contact_block' ): 
            get_template_part( 'inc/acf-content/contact-block' );
        elseif( get_row_layout() == 'banner_section' ): 
            get_template_part( 'inc/acf-content/banner-section' );
        elseif( get_row_layout() == 'tickets_offices_section' ): 
            get_template_part( 'inc/acf-content/tickets-offices-section' );
        elseif( get_row_layout() == 'partners_section' ): 
            get_template_part( 'inc/acf-content/partners-section' );
        elseif( get_row_layout() == 'timetable_section' ): 
            get_template_part( 'inc/acf-content/timetable-section' );
        elseif ( get_row_layout() == 'gallery_section' ) :
            get_template_part( 'inc/acf-content/gallery-section' );
        elseif ( get_row_layout() == 'routes_list' ) :
            get_template_part( 'inc/acf-content/routes-list' );
        elseif ( get_row_layout() == 'advertising_section' ) :
            get_template_part( 'inc/acf-content/advertising-section' );
        elseif ( get_row_layout() == 'how_work_section' ) :
            get_template_part( 'inc/acf-content/how-work-section' );
        elseif ( get_row_layout() == 'documents_list' ) :
                    get_template_part( 'inc/acf-content/documents-list' );
        elseif ( get_row_layout() == 'carrier_section' ) :
                        get_template_part( 'inc/acf-content/carrier-section' );
        endif;
    endwhile;
else :
    echo '
        <section class="page__section">
            <div class="page__content">
                <div class="container">
                    <div class="row">
                        <div class="col">
                            <div class="no__content">
                                <h1>'.__('Nothing to show', 'zebrabus').'</h1>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    ';
endif;

get_footer();
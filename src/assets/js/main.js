(function($) {
    // $(document).ready(function(){
    //     $('.preloader__wrapper .preloader__block .bus').addClass('stop');
    //     var count = $('.preloader__wrapper .preloader__block .percent .number');
    //     $({ Counter: 0 }).animate({ Counter: 70 }, {
    //       duration: 1500,
    //       step: function () {
    //         count.text(Math.ceil(this.Counter));
    //       }
    //     });
    // });

    $(window).on('load', function(){
        /*Preloader*/
        // var count = $('.preloader__wrapper .preloader__block .percent .number');
        
        // setTimeout(function(){
        //     $({ Counter: 71 }).animate({ Counter: 100 }, {
        //       duration: 1500,
        //       step: function () {
        //         count.text(Math.ceil(this.Counter));
        //       }
        //     });
        //     $('.preloader__wrapper .preloader__block .bus').addClass('go');
        //     $('.preloader__wrapper .preloader__block .people').addClass('hide');
        //     setTimeout(function(){
        //         $('.preloader__wrapper').addClass('hide');
        //     }, 1500);
        // }, 2000);
        $('.preloader__wrapper').addClass('hide');
        /**/
        $('select').select2({
            minimumResultsForSearch: -1
        });
        if($('.offices__block').length){
            $('select').on('select2:select', function (e) {
                var city = e.params.data.id;

                if( !city ){ 
                    $('.office__row').removeClass('hide');
                } else {
                    $('.office__row').each(function(){
                        var dataCity = $(this).attr('data-city');
                        if ( city == dataCity ) {
                            $(this).removeClass('hide');
                        } else {
                            $(this).addClass('hide');
                        }
                    });
                }
            });
        }

        /*Mobile menu*/
        $('.menu__btn').on('click', function(){
            $(this).toggleClass('show__menu');
            $('.mobile__menu').toggleClass('show__menu');
        });
        $('.mobile__menu .main__nav a').on('click', function(){
            $('.menu__btn').removeClass('show__menu');
            $('.mobile__menu').removeClass('show__menu');
        });
        /*Animate scroll*/
        // $(document).on('click', '.main__nav a, .contact__us, .learn__more', function (event) {
        //     var headerHeight = $('header').height();
        //     var link = $(this).attr('href');
        //     link = '#' + link.substring(link.indexOf("#") + 1);

        //     event.preventDefault();
        //     // $(this).addClass('active');
        //     // $('.main__nav a').not(this).removeClass('active');
        //     // $('#mobile__menu a').not(this).removeClass('active');

        //     $('html, body').animate({
        //         scrollTop: $(link).offset().top - headerHeight
        //     }, 500);
        // });
        // $(window).on('scroll', function(){
        //     let headerHeight = $('header').height(),
        //         topScroll = $(document).scrollTop() + headerHeight;
        
        //     $('.scroll').each(function(){
        //         var sectionScrol = $(this).offset().top,
        //             sectionHeight = $(this).height(),
        //             sectionActiveheight = sectionScrol + sectionHeight;
        //         if( topScroll + 10 >= sectionScrol && topScroll < sectionActiveheight ){
        //             var activeSection = '#' + $(this).attr('id');
        //             $('.main__nav li').each(function(){
        //                 var linkHref = $(this).find('a').attr('href');
        //                 if( linkHref == activeSection ){
        //                     $(this).find('a').addClass('active');
        //                 } else {
        //                     $(this).find('a').removeClass('active');
        //                 }
        //             });
        //         }
        //     })
        // });
        /*Timateble*/
        if($('.periods__wrapper').length){
            $('.period__date').on('click', function(){
                var classList = $(this).attr('class').split(' '),
                    tabClass = '.' + classList[1],
                    periodName = $(this).text();
                $(this).closest('.periods__nav').find('.current__period').text(periodName);
                $(this).closest('.periods__wrapper').find('.period').removeClass('active');
                $(this).closest('.periods__wrapper').find(tabClass).addClass('active');
            });
        }
        /*Information slider*/
        if($('.table__slider').length){
            $('.table__slider').slick({
                infinite:       true,
                autoplay:       true,
                autoplaySpeed:  8000,
                speed:          700,
                arrows:         true,
                dots:           true,
                fade:           false
            });
        }
        /*Paralax*/
        var scene = document.getElementById('parallax-scene');
        var parallaxInstance = new Parallax(scene, {
            selector: '.circle'
        });
        /*Tab*/
        $('.tab__nav .tab').on('click', function(){
            let tabID = $(this).data('tab'),
                tabNav = $(this).closest('.tab__nav'),
                tabBlock = $(this).closest('.tab__block'),
                tabContent = tabBlock.find('.tab__content');

            $(this).addClass('active');
            $(tabNav).find('.tab').not(this).removeClass('active');

            tabContent.find('.tab').each(function(){
                let contentID = $(this).data('tab');
                if( contentID == tabID){
                    $(this).addClass('active');
                } else {
                    $(this).removeClass('active');
                }
            });
        });

        if ($('.gallery__images').length) {
            $('.gallery__images').lightGallery({
                selector: '.gallery',
                download: false,
                counter: false
            });
        }

        if ($('.gallery__section').length) {
            $('.gallery__section').lightGallery({
                selector: '.gallery',
                download: false,
                counter: false
            });
        }

        /*Gallery*/
        $('.gallery__section .load__more').on('click', function(){
            let hideImages = $(this).closest('.gallery__section').find('.hide__image');
            hideImages.each(function(){
                $(this).removeClass('hide__image');
            });
            $(this).remove();
        });

        $('.show__drop__off').on('click', function(){
            $(this).addClass('hide');
            $('.drop__off').removeClass('hide');
        });

        /*Google map*/
        $(function() {
            var marker = [],
                infowindow = [],
                urlParams = new URLSearchParams(location.search),
                map, image = $('.map__container').attr('data-marker');

            function addMarker(location, index, name, contentstr) {
                marker[index] = new google.maps.Marker({
                    position: location,
                    map: map,
                    icon: image
                });
                marker[index].setMap(map);

                infowindow[index] = new google.maps.InfoWindow({
                    content: contentstr
                });
                google.maps.event.addListener(marker[index], 'click', function() {
                    infowindow[index].open(map, marker[index]);
                });
            }

            function initialize() {
                var lat = $('.map__container').attr("data-lat");
                var lng = $('.map__container').attr("data-lng");
                var myLatlng = new google.maps.LatLng(lat, lng);

                var setZoom = parseInt($('.map__container').attr("data-zoom"));

                var styles = [ ];

                var styledMap = new google.maps.StyledMapType(styles, { name: "Styled Map" });

                var mapOptions = {
                    zoom: setZoom,
                    disableDefaultUI: false,
                    scrollwheel: false,
                    zoomControl: true,
                    streetViewControl: true,
                    center: myLatlng
                };
                map = new google.maps.Map(document.getElementById("google__map"), mapOptions);

                map.mapTypes.set('map_style', styledMap);
                map.setMapTypeId('map_style');

                $('.map__sidebar li').on('click', function(e){
                    e.preventDefault();
                    var lat = $(this).data('lat'),
                        lng = $(this).data('lng'),
                        this_index = $('.map__sidebar li').index(this),
                        newLocation = new google.maps.LatLng(lat, lng);
                        map.setCenter(newLocation);
                        map.setZoom(15);
                        google.maps.event.trigger(marker[this_index], 'click');
                    if( $(this).hasClass('mobile') ){
                        let mapTop = $('.map__wrapper').offset().top;
                        $('html, body').animate({
                             scrollTop: mapTop - $('header').height()
                        }, 300);
                    }
                });

                $('.map__sidebar li').each(function() {
                    var mark_lat = $(this).attr('data-lat'),
                        mark_lng = $(this).attr('data-lng'),
                        this_index = $('.map__sidebar li').index(this),
                        mark_name = 'template_marker_' + this_index,
                        mark_locat = new google.maps.LatLng(mark_lat, mark_lng),
                        mark_book_label = ( $(this).attr('data-book-label') ) ? $(this).attr('data-book-label') : '',
                        mark_dep_label = ( $(this).attr('data-book-label') ) ? $(this).attr('data-dep-label') : '',
                        mark_google_label = ( $(this).attr('data-google-label') ) ? $(this).attr('data-google-label') : '',
                        mark_book = ( $(this).attr('data-book') ) ? '<a href="'+ $(this).attr('data-book') +'" target="_blank" class="book__btn">'+ mark_book_label +'</a>' : '',
                        mark_dep = ( $(this).attr('data-dep') ) ? '<a href="'+ $(this).attr('data-dep') +'" target="_blank">'+ mark_dep_label +'</a>' : '',
                        mark_google = ( $(this).attr('data-google') ) ? '<a href="'+ $(this).attr('data-google') +'" target="_blank">'+ mark_google_label +'</a>' : '',
                        mark_str = $(this).attr('data-text') + '<div class="link__row">'+ mark_book + mark_dep + mark_google + '</div>';
                    addMarker(mark_locat, this_index, mark_name, mark_str);
                });

                if( urlParams.has('stop') ){
                    let stopId = urlParams.get('stop');
                    $('.map__sidebar li').each(function(){
                        let thisId = $(this).data('id');
                        if(stopId == thisId) $(this).trigger('click');
                    });
                }
            }

            if ($('.map__container').length) {
                initialize();
            }
        });

        AOS.init();
    });
})(jQuery);
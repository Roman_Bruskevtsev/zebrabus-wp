<?php
/**
 *
 * @package WordPress
 * @subpackage Zebrabus
 * @since 1.0
 * @version 1.0
 */
get_header(); 
$background = (get_field('routes_banner', 'option')) ? ' style="background-image: url('.get_field('routes_banner', 'option').');"' : '';
?>
<section class="routes__list">
    <div class="container">
        <?php if( get_field('lines_title', 'option') ) { ?>
        <div class="row">
            <div class="col">
                <div class="section__title" data-aos="fade-up">
                    <h2><?php the_field('lines_title', 'option'); ?></h2>
                </div>
            </div>
        </div>
        <?php } 
        $routes_list = get_field('lines_list', 'option'); 
        if( $routes_list ) { ?>
        <div class="row">
            <?php foreach ( $routes_list as $route ) { ?>
            <div class="col-lg-12">
                <div class="route__description" data-aos="fade-up">
                    <div class="route__block">
                        <?php if( $route['main_stations'] ) { ?><h5><?php echo $route['main_stations']; ?></h5><?php } ?>
                        <?php if( $route['intermediate_stations'] ) { ?><p><?php echo $route['intermediate_stations']; ?></p><?php } ?>
                    </div>
                    <div class="dates__block">
                        <div class="float-right">
                            <h6><?php _e('Valid for:', 'zebrabus'); ?></h6>
                            <div class="dates"><?php echo $route['start_date']; ?> - <?php echo $route['end_date']; ?></div>
                        </div>
                    </div>
                    <div class="link__block">
                    <?php if( $route['link'] ) { 
                        $target = $route['link']['target'] ? ' target="'.$route['link']['target'].'"' : ''; ?>
                        <a class="btn green__btn" href="<?php echo $route['link']['url'] ?>"<?php echo $target; ?>><?php echo $route['link']['title'] ?></a>
                    <?php } ?>
                    </div>
                </div>
            </div>
            <?php } ?>
        </div>
        <?php } ?>
    </div>
</section>
<?php if( get_field('routes_title','option') ){ ?>
<div class="container">
    <div class="row">
        <div class="col">
            <div class="page__title" data-aos="fade-left">
                <h1><?php the_field('routes_title','option'); ?></h1>
            </div>
        </div>
    </div>
</div>
<?php } ?>
<?php if ( have_posts() ) : ?>
<div class="container">
    <div class="row">
    <?php while ( have_posts() ) : the_post();
        get_template_part( 'template-parts/routes/content');
    endwhile; ?>
    </div>
</div>
<?php endif; ?>
<div class="routes_banner"<?php echo $background; ?>>
    <div class="container">
        <div class="row">
            <div class="col">
                <div class="content" data-aos="fade-up">
                    <div class="text">
                        <?php if( get_field('routes_banner_icon', 'option') ) { 
                        $icon = get_field('routes_banner_icon', 'option');
                        ?>
                        <img src="<?php echo $icon['url']; ?>" alt="<?php echo $icon['title']; ?>">
                        <?php } 
                        if( get_field('routes_banner_title', 'option') ) { ?>
                        <h2><?php the_field('routes_banner_title', 'option'); ?></h2>
                        <?php }
                        if( get_field('routes_button_url', 'option') ) { ?>
                        <a class="btn green__btn" href="<?php the_field('routes_button_url', 'option'); ?>"><?php the_field('routes_button_label', 'option'); ?></a>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
$groups = get_field('group_trips', 'option'); 
if( $groups ) { ?>
<section class="group__trips">
    <div class="container">
        <?php if( $groups['title'] ) { ?>
        <div class="row">
            <div class="col">
                <div class="section__title" data-aos="fade-up">
                    <h2><?php echo $groups['title']; ?></h2>
                </div>
            </div>
        </div>
        <?php } 
        $trips = $groups['choose_group_trips']; 
        if( $trips ) { 
            $args = array(
                'posts_per_page'    => -1,
                'post__in'          => $trips,
                'post_type'         => 'group-trips',
                'orderby'           => 'post__in'
            );

            $query = new WP_Query( $args ); 

            if ( $query->have_posts() ) { ?>

            <div class="row">
                <?php while ( $query->have_posts() ) { $query->the_post();
                    $background = ( get_the_post_thumbnail( get_the_ID() ) ) ? ' style="background-image: url('.get_the_post_thumbnail_url( get_the_ID(), 'gallery-thumbnail' ).');"' : ''; ?>
                    <div class="col-md-6 col-lg-4">
                        <a class="trip__block" data-aos="fade-up" href="<?php the_permalink(); ?>">
                            <div class="thumbnail"<?php echo $background; ?>>
                                <?php 
                                $popular = get_field('popular_route');
                                if( $popular ) { ?>
                                    <span><?php _e('Popular route', 'zebrabus'); ?></span>
                                <?php } ?>
                            </div>
                            <h3><?php the_title(); ?></h3>
                            <?php if( get_field('location') ) { ?>
                                <h3><b><?php the_field('location'); ?></b></h3>
                            <?php } ?>
                        </a>
                    </div>
                <?php } ?>
            </div>
            <?php }
            wp_reset_postdata();
        } 
        $quote_block = get_field('quote_block', 'option');
        if( $quote_block ) { ?>
            <div class="row">
                <div class="quote__block">
                    <div class="text" data-aos="fade-up">
                    <?php if( $quote_block['title'] ) { ?>
                        <h2><?php echo $quote_block['title']; ?></h2>
                    <?php } ?>
                    <?php if( $quote_block['subtitle'] ) { ?>
                        <h3><?php echo $quote_block['subtitle']; ?></h3>
                    <?php } 
                    if( $quote_block['link'] ) { 
                        $target = $quote_block['link']['target'] ? ' target="'.$quote_block['link']['target'].'"' : '';?>
                        <a class="btn green__btn shadow" href="<?php echo $quote_block['link']['url']; ?>"><?php echo $quote_block['link']['title']; ?></a>
                    <?php } ?>
                    </div>
                    <div class="zebrabus__block" data-aos="fade-left">
                        <h2 class="slogan"><?php _e('Traveling can be fun :)', 'zebrabus'); ?></h2>
                        <img src="<?php echo get_template_directory_uri().'/assets/images/bus_large.svg'; ?>" alt="<?php _e('Zebrabus', 'zebrabus'); ?>">
                    </div>
                </div>
            </div>
        <?php } ?>
    </div>
</section>
<?php }
get_footer();

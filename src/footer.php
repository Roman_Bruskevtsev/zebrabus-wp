<?php
/**
 *
 * @package WordPress
 * @subpackage Zebrabus
 * @since 1.0
 * @version 1.0
 */
?>  
    </main>
    <footer>
        <div class="top__line">
            <div class="container">
                <div class="row">
                    <?php if ( is_active_sidebar( 'footer-1' ) ) { ?>
                    <div class="col-lg-4">
                        <div class="navigation__widget">
                            <?php dynamic_sidebar( 'footer-1' ); ?>
                        </div>
                    </div>
                    <?php } ?>
                    <?php if ( is_active_sidebar( 'footer-2' ) ) { ?>
                    <div class="col-lg-4">
                        <div class="navigation__widget padding__left">
                            <?php dynamic_sidebar( 'footer-2' ); ?>
                        </div>
                    </div>
                    <?php } ?>
                    <?php if ( is_active_sidebar( 'footer-3' ) ) { ?>
                    <div class="col-sm-6 col-md-6 col-lg-2">
                        <div class="contact__widget">
                            <?php dynamic_sidebar( 'footer-3' ); ?>
                        </div>
                    </div>
                    <?php } ?>
                    <?php if ( is_active_sidebar( 'footer-4' ) ) { ?>
                    <div class="col-sm-6 col-md-6 col-lg-2">
                        <div class="social__widget">
                            <?php dynamic_sidebar( 'footer-4' ); ?>
                        </div>
                    </div>
                    <?php } ?> 
                </div>
            </div>
            
        </div>
        <div class="bottom__line">
            <div class="container">
                <div class="row">
                    <div class="col">
                        <?php if( get_field('copyrights', 'option') ) { ?>
                        <div class="copyrights text-center">
                            <p><?php the_field('copyrights', 'option'); ?></p>
                        </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <?php wp_footer(); ?>
</body>
</html>
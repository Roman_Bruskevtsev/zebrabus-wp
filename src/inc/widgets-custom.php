<?php 

function zebrabus_load_widget() {
    register_widget( 'zebrabus_contact_widget' );
    register_widget( 'zebrabus_social_widget' );
}
add_action( 'widgets_init', 'zebrabus_load_widget' );
 
// Creating the widget 
class zebrabus_contact_widget extends WP_Widget {
 
    function __construct() {
        parent::__construct(
            'zebrabus_contact_widget', 
            __('Zebrabus Contact Widget', 'zebrabus'), 
            array( 'description' => __( 'Contact widget for Zebrabus', 'zebrabus' ), ) 
        );
    }
    
    public function widget( $args, $instance ) {
        $title = apply_filters( 'widget_title', $instance['title'] );

        echo $args['before_widget'];
        if ( ! empty( $title ) ) 
        echo $args['before_title'] . $title . $args['after_title']; ?>
        <ul class="contact__list">
            <?php if( $instance['phone'] ) { ?>
            <li>
                <a href="tel:<?php echo $instance['phone']; ?>" class="phone"><?php echo $instance['phone']; ?></a>
            </li>
            <?php } ?>
            <?php if( $instance['email'] ) { ?>
            <li>
                <a href="mailto:<?php echo $instance['email']; ?>" class="email"><?php echo $instance['email']; ?></a>
            </li>
            <?php } ?>
        </ul>
        
        <?php echo $args['after_widget'];
    }
             
    public function form( $instance ) {
        if ( isset( $instance[ 'title' ] ) ) {
            $title = $instance[ 'title' ];
        }
        else {
            $title = __( 'New title', 'zebrabus' );
        } ?>
        <p>
            <label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:', 'zebrabus' ); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
        </p>
        <p>
            <label for="<?php echo $this->get_field_id( 'phone' ); ?>"><?php _e( 'Phone:', 'zebrabus' ); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id( 'phone' ); ?>" name="<?php echo $this->get_field_name( 'phone' ); ?>" type="text" value="<?php echo esc_attr( $instance[ 'phone' ] ); ?>" />
        </p>
        <p>
            <label for="<?php echo $this->get_field_id( 'email' ); ?>"><?php _e( 'Email:', 'zebrabus' ); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id( 'email' ); ?>" name="<?php echo $this->get_field_name( 'email' ); ?>" type="email" value="<?php echo esc_attr( $instance[ 'email' ] ); ?>" />
        </p>
        <?php
    }

    public function update( $new_instance, $old_instance ) {
        $instance = array();
        $instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
        $instance['phone'] = ( ! empty( $new_instance['phone'] ) ) ? strip_tags( $new_instance['phone'] ) : '';
        $instance['email'] = ( ! empty( $new_instance['email'] ) ) ? strip_tags( $new_instance['email'] ) : '';
        return $instance;
    }
}

class zebrabus_social_widget extends WP_Widget {
 
    function __construct() {
        parent::__construct(
            'zebrabus_social_widget', 
            __('Zebrabus Social Widget', 'zebrabus'), 
            array( 'description' => __( 'Social widget for Zebrabus', 'zebrabus' ), ) 
        );
    }
    
    public function widget( $args, $instance ) {
        $title = apply_filters( 'widget_title', $instance['title'] );

        echo $args['before_widget'];
        if ( ! empty( $title ) ) 
        echo $args['before_title'] . $title . $args['after_title']; ?>
        <ul class="social__list">
            <?php if( $instance['instagram'] ) { ?>
            <li>
                <a href="<?php echo $instance['instagram']; ?>" target="_blank" class="instagram"></a>
            </li>
            <?php } ?>
            <?php if( $instance['facebook'] ) { ?>
            <li>
                <a href="<?php echo $instance['facebook']; ?>" target="_blank" class="facebook"></a>
            </li>
            <?php } ?>
        </ul>
        
        <?php echo $args['after_widget'];
    }
             
    public function form( $instance ) {
        if ( isset( $instance[ 'title' ] ) ) {
            $title = $instance[ 'title' ];
        }
        else {
            $title = __( 'New title', 'zebrabus' );
        } ?>
        <p>
            <label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:', 'zebrabus' ); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
        </p>
        <p>
            <label for="<?php echo $this->get_field_id( 'instagram' ); ?>"><?php _e( 'Instagram:', 'zebrabus' ); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id( 'instagram' ); ?>" name="<?php echo $this->get_field_name( 'instagram' ); ?>" type="text" value="<?php echo esc_attr( $instance[ 'instagram' ] ); ?>" />
        </p>
        <p>
            <label for="<?php echo $this->get_field_id( 'facebook' ); ?>"><?php _e( 'Facebook:', 'zebrabus' ); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id( 'facebook' ); ?>" name="<?php echo $this->get_field_name( 'facebook' ); ?>" type="text" value="<?php echo esc_attr( $instance[ 'facebook' ] ); ?>" />
        </p>
        <?php
    }

    public function update( $new_instance, $old_instance ) {
        $instance = array();
        $instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
        $instance['instagram'] = ( ! empty( $new_instance['instagram'] ) ) ? strip_tags( $new_instance['instagram'] ) : '';
        $instance['facebook'] = ( ! empty( $new_instance['facebook'] ) ) ? strip_tags( $new_instance['facebook'] ) : '';
        return $instance;
    }
}
<?php
add_action('init', 'zebrabus_routes_post', 0);

function zebrabus_routes_post() {
  //Register taxonomy
  $taxonomy_labels = array(
    'name'                        => __('Routes categories','zebrabus'),
    'singular_name'               => __('Route category','zebrabus'),
    'menu_name'                   => __('Routes categories','zebrabus'),
  );

  $taxonomy_rewrite = array(
    'slug'                  => 'routes-categories',
    'with_front'            => true,
    'hierarchical'          => true,
  );

  $taxonomy_args = array(
    'labels'              => $taxonomy_labels,
    'hierarchical'        => true,
    'public'              => true,
    'show_ui'             => true,
    'show_admin_column'   => true,
    'show_in_nav_menus'   => true,
    'show_tagcloud'       => true,
    'rewrite'             => $taxonomy_rewrite,
  );
  register_taxonomy( 'routes-categories', 'routes', $taxonomy_args );
  
  //Register new post type
  $post_labels = array(
  'name'                => __('Routes', 'zebrabus'),
  'add_new'             => __('Add Route', 'zebrabus')
  );

  $post_args = array(
  'label'               => __('Routes', 'zebrabus'),
  'description'         => __('Routes information page', 'zebrabus'),
  'labels'              => $post_labels,
  'supports'            => array( 'title', 'thumbnail'),
  'taxonomies'          => array( 'routes-categories' ),
  'hierarchical'        => false,
  'public'              => true,
  'show_ui'             => true,
  'show_in_menu'        => true,
  'has_archive'         => true,
  'can_export'          => true,
  'show_in_nav_menus'   => true,
  'publicly_queryable'  => true,
  'exclude_from_search' => false,
  'query_var'           => true,
  'capability_type'     => 'post',
  'menu_icon'           => 'dashicons-location-alt'
  );
  register_post_type( 'routes', $post_args );
}

add_action('init', 'zebrabus_stops_post', 0);

function zebrabus_stops_post(){
  $post_labels = array(
  'name'                => __('Stops', 'zebrabus'),
  'add_new'             => __('Add Stop', 'zebrabus')
  );

  $post_args = array(
  'label'               => __('Stops', 'zebrabus'),
  'description'         => __('Stops information page', 'zebrabus'),
  'labels'              => $post_labels,
  'supports'            => array( 'title'),
  'taxonomies'          => array( '' ),
  'hierarchical'        => false,
  'public'              => true,
  'show_ui'             => true,
  'show_in_menu'        => true,
  'has_archive'         => true,
  'can_export'          => true,
  'show_in_nav_menus'   => true,
  'publicly_queryable'  => true,
  'exclude_from_search' => false,
  'query_var'           => true,
  'rewrite'             => '',
  'capability_type'     => 'post',
  'menu_icon'           => 'dashicons-location'
  );
  register_post_type( 'stops', $post_args );
}

add_action('init', 'zebrabus_group_trips', 0);

function zebrabus_group_trips(){
  $post_labels = array(
  'name'                => __('Group Trips', 'zebrabus'),
  'add_new'             => __('Add Group Trip', 'zebrabus')
  );

  $post_args = array(
  'label'               => __('Group Trips', 'zebrabus'),
  'description'         => __('Group Trips information page', 'zebrabus'),
  'labels'              => $post_labels,
  'supports'            => array( 'title', 'thumbnail'),
  'taxonomies'          => array( '' ),
  'hierarchical'        => false,
  'public'              => true,
  'show_ui'             => true,
  'show_in_menu'        => true,
  'has_archive'         => false,
  'can_export'          => true,
  'show_in_nav_menus'   => true,
  'publicly_queryable'  => true,
  'exclude_from_search' => false,
  'query_var'           => true,
  'rewrite'             => '',
  'capability_type'     => 'post',
  'menu_icon'           => 'dashicons-groups'
  );
  register_post_type( 'group-trips', $post_args );
}

add_action('init', 'zebrabus_vacancies', 0);

function zebrabus_vacancies(){
  $post_labels = array(
  'name'                => __('Vacancies', 'zebrabus'),
  'add_new'             => __('Add Vacancy', 'zebrabus')
  );

  $post_args = array(
  'label'               => __('Vacancies', 'zebrabus'),
  'description'         => __('Vacancy information page', 'zebrabus'),
  'labels'              => $post_labels,
  'supports'            => array( 'title', 'thumbnail'),
  'taxonomies'          => array( '' ),
  'hierarchical'        => false,
  'public'              => true,
  'show_ui'             => true,
  'show_in_menu'        => true,
  'has_archive'         => false,
  'can_export'          => true,
  'show_in_nav_menus'   => true,
  'publicly_queryable'  => true,
  'exclude_from_search' => false,
  'query_var'           => true,
  'rewrite'             => '',
  'capability_type'     => 'post',
  'menu_icon'           => 'dashicons-groups'
  );
  register_post_type( 'vacancies', $post_args );
}
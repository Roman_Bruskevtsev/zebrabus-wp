<section class="news__section gradient padding scroll">
    <div class="circle__container">
        <div class="circle__1 circle" data-depth="0.2"></div>
    </div>
    <div class="container">
        <?php if( get_sub_field('title') ) { ?>
        <div class="row">
            <div class="col">
                <div class="section__title" data-aos="fade-up">
                    <h2><?php the_sub_field('title'); ?></h2>
                </div>
            </div>
        </div>
        <?php } ?>
        <?php 
        $args = array(
            'posts_per_page' => 2,
            'orderby'        => 'date'
        );
        $query = new WP_Query( $args );
        if ( $query->have_posts() ) { ?>
            <div class="row">
            <?php while ( $query->have_posts() ) { $query->the_post();
                get_template_part( 'template-parts/post/content');
            } 
            get_template_part( 'template-parts/post/content-more');
            ?>
            </div>
        <?php } wp_reset_postdata(); ?>
    </div>
    <div class="container">
        <div class="row">
            <div class="col">
                <div class="subscribe__block">
                    <div class="reading__zebra" data-aos="fade-up"></div>
                    <?php if( get_sub_field('subscribe_text') ) {?>
                    <div class="quotes__block" data-aos="fade-up">
                        <h5 class="slogan"><?php the_sub_field('subscribe_text'); ?></h5>
                    </div>
                    <?php } 
                    if(get_sub_field('subscribe_form')) { ?>
                        <div class="subscribe__form" data-aos="fade-up">
                            <span class="arrow__block"></span>
                            <span class="line__icon"></span>
                            <?php echo do_shortcode( get_sub_field('subscribe_form') ); ?>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="container">
    <div class="row" id="ticket-offices">
        <div class="col">
            <div class="offices__title">
                <?php if( get_sub_field('title') ) { ?><h2 data-aos="fade-up"><?php the_sub_field('title'); ?></h2><?php } ?>
                <?php if( have_rows('offices') ): 
                    $cities = [];
                    while ( have_rows('offices') ) : the_row();
                        if(!in_array( get_sub_field('city'), $cities )) array_push( $cities, get_sub_field('city') );
                    endwhile;
                ?>
                <div class="cities__nav" data-aos="fade-left">
                    <label>
                        <span class="title"><?php _e('Select city:', 'zebrabus'); ?></span>
                        <select>
                            <option value=""><?php _e('All cities', 'zebrabus'); ?></option>
                        <?php foreach ($cities as $city) { ?>
                            <option value="<?php echo zebra_slug($city); ?>"><?php echo $city; ?></option>
                        <?php } ?> 
                        </select>
                        
                    </label>
                </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
    <?php if( have_rows('offices') ): ?>
    <div class="row">
        <div class="col">
            <div class="offices__block">
                <div class="offices__heading" data-aos="fade-up">
                    <div class="type column">
                        <span><?php _e('Type', 'zebrabus'); ?></span>
                    </div>
                    <div class="address column">
                        <span><?php _e('Address:', 'zebrabus'); ?></span>
                    </div>
                    <div class="phone column">
                        <span><?php _e('Phone number:', 'zebrabus'); ?></span>
                    </div>
                    <div class="timetable column">
                        <span><?php _e('Opening hours:', 'zebrabus'); ?></span>
                    </div>
                    <div class="map column">
                    </div>
                </div>
                <div class="offices__body">
                <?php while ( have_rows('offices') ) : the_row(); ?>
                    <div class="office__row" data-city="<?php echo zebra_slug( get_sub_field('city') ); ?>">
                        <div class="office__content">
                            <div class="type column">
                                <span><?php the_sub_field('type'); ?></span>
                            </div>
                            <div class="address column">
                                <span><?php the_sub_field('address'); ?></span>
                            </div>
                            <div class="phone column">
                                <?php if( get_sub_field('phone') ) { ?>
                                    <a href="tel:<?php the_sub_field('phone'); ?>">
                                        <?php the_sub_field('phone'); ?>    
                                    </a>
                                <?php } ?>
                            </div>
                            <div class="timetable column">
                                <?php if( get_sub_field('timetable') ) { ?>
                                    <span><?php the_sub_field('timetable'); ?></span>
                                <?php } ?>
                            </div>
                            <div class="map column text-right">
                            <?php if( get_sub_field('map_link') ) { ?>
                                <a href="<?php the_sub_field('map_link'); ?>" target="_blank">
                                    <?php _e('Show on map', 'zebrabus'); ?>   
                                </a>
                            <?php } ?>
                            </div>
                        </div>
                    </div>
                <?php endwhile; ?>
                </div>
            </div>
        
        </div>
    </div>
    <?php endif;
    $busline_block = get_sub_field('pricelist_for_busline');
    if( $busline_block ) { ?>
    <div class="row">
        <div class="col-lg-12">
            <div class="busline__block margin">
                <?php if( $busline_block['title'] ) { ?>
                    <h3 data-aos="fade-left"><?php echo $busline_block['title']; ?></h3>
                <?php } 
                $buslines = $busline_block['buslines']; 
                if( $buslines ) { 
                    foreach ($buslines as $line) { ?>
                        <div class="busline__row" data-aos="fade-up">
                            <div class="icon"></div>
                            <div class="stations"><h5><?php echo $line['stations']; ?></h5></div>
                            <div class="link text-right">
                            <?php if( $line['link'] ) { 
                                $target = $line['link']['target'] ? ' target="'.$line['link']['target'].'"' : ''; ?>
                                <a class="btn black__btn" href="<?php echo $line['link']['url'] ?>"<?php echo $target; ?>><?php echo $line['link']['title'] ?></a>
                            <?php } ?>
                            </div>
                        </div>
                    <?php }
                } ?>
            </div>
        </div>
    </div>
    <?php } 
    $additional = get_sub_field('additional_information');
    if($additional){ ?>
    <?php if( $additional['title'] ){ ?>
    <div class="row" id="change-and-cancel-tickets">
        <div class="col">
            <h2 data-aos="fade-left"><?php echo $additional['title']; ?></h2>
        </div>
    </div>
    <?php }
    if( $additional['text'] ){ ?>
    <div class="row">
        <div class="col-lg-6">
            <div class="additional__text" data-aos="fade-up">
                <?php echo $additional['text']; ?>
            </div>
        </div>
    </div>
    <?php } ?>
</div>
<div class="zebra__quotes__block" data-aos="fade-left">
    <div class="text">
        <?php if( $additional['zebra_text'] ){ ?>
        <h4><?php echo $additional['zebra_text']; ?></h4>
        <?php }
        if( $additional['contact_link'] ){ ?>
        <a href="<?php echo $additional['contact_link']; ?>" class="btn simple__btn contact__us">
            <?php echo $additional['contact_label']; ?></a>
        <?php } ?>
    </div>
    <?php if( $additional['zebra_image'] ){ 
        $zebra = $additional['zebra_image'];
    ?>
    <div class="image">
        <img src="<?php echo $zebra['url']; ?>" alt="<?php echo $zebra['title']; ?>">
    </div>
    <?php } ?>
</div>
<?php } ?>
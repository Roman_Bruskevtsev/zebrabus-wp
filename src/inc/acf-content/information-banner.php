<section class="information__section">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-lg-6">
                <div class="content">
                    <?php if( get_sub_field('title') ) { ?>
                        <h1 data-aos="fade-up"><?php the_sub_field('title'); ?></h1>
                    <?php } ?>
                    <?php if( have_rows('benefits_list') ): ?>
                    <div class="benefits__list">
                        <?php while ( have_rows('benefits_list') ) : the_row(); ?>
                        <div class="benefit" data-aos="fade-up">
                            <?php if( get_sub_field('icon') ) { ?>
                                <img src="<?php echo get_sub_field('icon')['url']; ?>" alt="<?php echo get_sub_field('icon')['title']; ?>">
                            <?php } ?>
                            <?php if( get_sub_field('title') ) { 
                                if( get_sub_field('add_link') ) { ?>
                                    <a href="<?php the_sub_field('link'); ?>">
                                <?php } ?>
                                        <h5><?php the_sub_field('title'); ?></h5>
                                <?php if( get_sub_field('add_link') ) { ?>
                                    <span class="link"></span>
                                    </a>
                                <?php }
                            } ?>
                        </div>
                        <?php endwhile; ?>
                    </div>
                    <?php endif; ?>
                    <div class="btn__row">
                        <?php if( get_sub_field('green_button_link') ) { ?>
                        <a href="<?php the_sub_field('green_button_link'); ?>" class="btn green__btn shadow float-left" data-aos="fade-up"><?php the_sub_field('green_button_label'); ?></a>
                        <?php } ?>
                        <?php if( get_sub_field('white_button_link') ) { ?>
                        <a href="<?php the_sub_field('white_button_link'); ?>" class="btn simple__btn shadow float-left learn__more" data-aos="fade-up"><?php the_sub_field('white_button_label'); ?></a>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php if( get_sub_field('image') ) { ?>
    <div class="image" data-aos="fade-left" data-aos-duration="1000">
        <img src="<?php echo get_sub_field('image')['url']; ?>" alt="<?php echo get_sub_field('image')['title']; ?>">
    </div>
    <?php } ?>
</section>

<?php
$banner = get_sub_field('banner');
?>
<div class="row">
    <div class="col-lg-6">
        <?php if( get_sub_field('text') ) { ?>
        <div class="banner__text" data-aos="fade-up">
            <?php the_sub_field('text'); ?>
        </div>
        <?php } ?>
    </div>
    <div class="col-lg-1"></div>
    <div class="col-lg-5">
        <?php if( $banner ) {  ?>
        <div class="banner__image" data-aos="fade-up">
            <div class="title__row">
            <?php if( $banner['title'] ) { ?>
                <h5><?php echo $banner['title']; ?></h5>
            <?php } ?>
            <?php if( $banner['button_link'] ) { ?>
                <a href="<?php echo $banner['button_link']; ?>" class="details__btn"><?php echo $banner['button_label']; ?></a>
            <?php } ?>
            </div>
            <?php if( $banner['image'] ) { ?>
            <img src="<?php echo $banner['image']['url']; ?>" width="<?php echo $banner['image']['width']; ?>" height="<?php echo $banner['image']['height']; ?>" alt="<?php echo $banner['image']['title']; ?>">
            <?php } ?>
        </div>
        <?php } ?>
    </div>
</div>
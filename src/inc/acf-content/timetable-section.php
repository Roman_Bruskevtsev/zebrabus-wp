<div class="additional__information small__padding gradient" id="timetable">
    <div class="circle__container">
        <div class="circle__3 circle" data-depth="0.2"></div>
        <div class="circle__4 circle" data-depth="0.3"></div>
    </div>
    <div class="container">
        <?php if( get_sub_field('title') ) { ?>
        <div class="row">
            <div class="col">
                <h2 data-aos="fade-left"><?php echo get_sub_field('title'); ?></h2>
            </div>
        </div>
        <?php } 
        if( get_sub_field('periods') ) { ?>
        <div class="periods__wrapper" data-aos="fade-up">
            <?php 
            $i = $j = $k = 0;
            $period_dates = []; ?>
            <div class="periods__nav">
                <?php foreach ( get_sub_field('periods') as $period ) { 
                    if( $j == 0 ) { ?>
                    <div class="current__period"><?php echo $period['time']; ?></div>
                    <?php }
                 $j++; } ?>
                <div class="periods__list">
                <?php foreach ( get_sub_field('periods') as $period ) { ?>
                    <div class="period__date<?php echo $nav; ?> period__<?php echo $k; ?>"><?php echo $period['time']; ?></div>
                <?php $k++; } ?>
                </div>
            
            </div>
            <div class="periods__block">
                <?php foreach ( get_sub_field('periods') as $period ) { 
                    $status = ($i == 0) ? ' active' : '';
                ?>
                <div class="period period__<?php echo $i; echo $status; ?>">
                    <?php if( $period['information'] ) { ?>
                    <div class="period__information">
                        <p><?php echo $period['information']; ?></p>
                    </div>
                    <?php } 
                    if( $period['directions_group'] ) { ?>
                    <div class="directions__group">
                        <?php foreach ( $period['directions_group'] as $group ) { ?>
                            <div class="directions">
                                <div class="title">
                                    <?php if( $group['title'] ) { ?><h5><?php echo $group['title']; ?></h5><?php } ?>
                                    <div class="description"><?php echo $group['routes_description']; ?></div>
                                    <div class="daily__time text-right">
                                    <?php if( $group['show_daily_time'] ) { ?>
                                        <span class="text"><?php echo $group['daily_time_label']; ?></span>
                                        <?php if( $group['daily_time_description'] ) { ?>
                                        <span class="info__window"><?php echo $group['daily_time_description']; ?></span>
                                        <?php } ?>
                                    <?php } ?>
                                    </div>
                                </div>
                                <?php if( $group['directions'] ) { ?>
                                <div class="directions__rows">
                                <?php foreach ( $group['directions'] as $row ) { ?>
                                    <div class="direction__row">
                                        <div class="name"><?php echo $row['name']; ?></div>
                                        <?php if( $row['stops'] ) { ?>
                                        <div class="map__link">
                                            <a href="<?php echo esc_url( home_url( '/stops/?stop=' ) ). $row['stops']; ?>" target="_blank"><?php _e('Show on map', 'zebrabus'); ?></a>
                                        </div>
                                        <?php } ?>
                                        <div class="status"><?php echo $row['status']; ?></div>
                                        <div class="daily__time text-right">
                                            <?php echo $row['daily_time']; ?>
                                        </div>
                                    </div>
                                <?php } ?>
                                </div>
                                <?php } ?>
                            </div>
                        <?php } ?>
                    </div>
                    <?php } ?>
                </div>
                <?php $i++; } ?>
            </div>
        </div>
        <?php } ?>
    </div>
</div>
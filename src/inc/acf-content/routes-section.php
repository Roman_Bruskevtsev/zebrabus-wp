<section class="routes__section">
    <div class="container">
        <?php if( get_sub_field('title') ) { ?>
        <div class="row">
            <div class="col">
                <div class="section__title" data-aos="fade-up">
                    <h2><?php the_sub_field('title'); ?></h2>
                </div>
            </div>
        </div>
        <?php } ?>
        <?php if( get_sub_field('choose_routes') ){
            $posts = get_sub_field('choose_routes');
            $args = array(
                'post_type' => 'routes',
                'post__in'  => $posts,
                'orderby'   => 'post__in'
            );
            $query = new WP_Query( $args );
            if ( $query->have_posts() ) { ?>
                <div class="row">
                <?php while ( $query->have_posts() ) { $query->the_post();
                    get_template_part( 'template-parts/routes/content');
                } ?>
                </div>
                <?php if( get_sub_field('all_routes_button_label') ){ ?>
                <div class="row">
                    <div class="col">
                        <div class="archive__link text-center" data-aos="fade-up">
                            <a href="<?php echo get_post_type_archive_link( 'routes' ); ?>" class="btn simple__btn shadow"><?php the_sub_field('all_routes_button_label'); ?></a>
                        </div>
                    </div>
                </div>
                <?php } ?>
            <?php } wp_reset_postdata(); ?>

        <?php } ?>
    </div>
</section>
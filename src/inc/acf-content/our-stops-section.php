<div class="container">
    <?php if( have_rows('stops') ): ?>
    <div class="row">
        <div class="col-md-4 col-lg-4 col-xl-3">
            <div class="map__sidebar" data-aos="fade-up">
                <ul class="stops">
                <?php while ( have_rows('stops') ) : the_row(); ?>
                    <?php if( get_sub_field('latitude') && get_sub_field('longitude') && get_sub_field('name') ) { ?>
                    <li 
                    data-lat="<?php the_sub_field('latitude'); ?>"
                    data-lng="<?php the_sub_field('longitude'); ?>"
                    data-text="<?php the_sub_field('description'); ?>"
                    data-book="<?php the_sub_field('book_link'); ?>"
                    data-dep="<?php the_sub_field('departures_link'); ?>"
                    data-google="<?php the_sub_field('google_maps'); ?>">
                        <span class="title"><?php the_sub_field('name'); ?></span>
                        <span class="link"><?php _e( 'Show on map', 'zebrabus' ); ?></span>
                    </li>
                    <?php } ?>
                <?php endwhile; ?>
                </ul>
            </div>
        </div>
    </div>
    <?php endif; ?>
</div>
<?php
$map = get_sub_field('map');
if( $map ) { ?>
<div class="container-fluid map__section">
    <div class="row">
        <div class="offset-md-4 col-md-8 col-lg-8">
            <div class="map__container" data-lat="<?php echo $map['latitude']; ?>" data-lng="<?php echo $map['longitude']; ?>" data-zoom="<?php echo $map['zoom']; ?>" data-marker="<?php echo $map['marker']; ?>">
                <div id="google__map" class="map__wrapper"></div>
            </div>
        </div>
    </div>
</div>
<?php } ?>
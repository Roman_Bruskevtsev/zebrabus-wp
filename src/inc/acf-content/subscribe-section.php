<?php 
$posts_page = (int) get_option('page_for_posts');
$subscribe_block = get_field('subcribe_block', $posts_page);
if( $subscribe_block ) { ?>
<section class="gradient small__padding">
    <div class="container">
        <div class="row">
            <div class="col">
                <div class="subscribe__block">
                    <div class="reading__zebra" data-aos="fade-up"></div>
                    <?php if( $subscribe_block['text'] ) { ?>
                    <div class="quotes__block" data-aos="fade-up">
                        <h5 class="slogan"><?php echo $subscribe_block['text']; ?></h5>
                    </div>
                    <?php } 
                    if( $subscribe_block['form_shortcode'] ) { ?>
                        <div class="subscribe__form" data-aos="fade-up">
                            <span class="arrow__block"></span>
                            <span class="line__icon"></span>
                            <?php echo do_shortcode( $subscribe_block['form_shortcode'] ); ?>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</section>
<?php } ?>
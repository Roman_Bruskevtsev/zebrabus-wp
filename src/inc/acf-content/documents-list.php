<?php 
$documents_list = get_sub_field('documents');
if( $documents_list ){ ?>
<section class="documents__list">
	<div class="container">
		<div class="row">
			<div class="col">
				<div class="documents">
				<?php foreach ($documents_list as $document) { 
					$link = $document['link']; 
					$target = $link['target'] ? ' target="'.$link['target'].'"' : ''; ?>
					<div class="document">
						<h4><?php echo $document['title']; ?></h4>
						<?php if( $link ) { ?>
							<a class="btn black__btn" href="<?php echo $link['url']; ?>"<?php echo $target; ?>><?php echo $link['title']; ?></a>
						<?php } ?>
					</div>
				<?php } ?>
				</div>
			</div>
		</div>
	</div>
</section>
<?php } ?>
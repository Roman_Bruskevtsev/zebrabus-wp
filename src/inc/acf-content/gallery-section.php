<section class="gallery__section">
	<div class="container">
        <?php if( get_sub_field('title') ) { ?>
        <div class="row">
            <div class="col">
                <div class="section__title" data-aos="fade-up">
                    <h2><?php the_sub_field('title'); ?></h2>
                </div>
            </div>
        </div>
        <?php } 
        $images = get_sub_field('images'); 
        if( $images ) { 
            $i = 0; ?>
        	<div class="row">
    		<?php foreach ($images as $image) { 
                $class = $i > 5 ? ' hide__image' : ''; ?>
    			<div class="col-md-6 col-lg-4<?php echo $class; ?>">
			        <a href="<?php echo $image['url'] ?>" class="gallery" data-aos="fade-up">
			            <img src="<?php echo $image['sizes']['gallery-thumbnail'] ?>" width="<?php echo $image['sizes']['gallery-thumbnail-width']; ?>" height="<?php echo $image['sizes']['gallery-thumbnail-height']; ?>" alt="<?php echo $image['title'] ?>">
			        </a>
    			</div>
    		<?php $i++; } ?>
        	</div>
            <?php
             if( $i > 6 ) { ?>
            <div class="row">
                <div class="col-lg-12">
                    <div class="load__more text-center" data-aos="fade-up">
                        <button class="btn simple__btn"><?php _e('Load more photos', 'zebrabus'); ?></button>
                    </div>
                </div>
            </div>
            <?php } ?>
        <?php } 
        $show_additional = get_sub_field('show_additional_block');
        if( $show_additional ) { 
        	$additional = get_sub_field('additional_block'); 
        	if( $additional ) { ?>
        	<div class="row">
        		<div class="col-lg-12">
        			<div class="advertising__block">
        				<div class="title text-center">
        					<?php if( $additional['title'] ) { ?><h2><?php echo $additional['title']; ?></h2><?php } ?>
        					<?php if( $additional['subtitle'] ) { ?><h3><?php echo $additional['subtitle']; ?></h3><?php } ?>
        				</div>
        				<?php if( $additional['link_1'] || $additional['link_2'] ) { 
        					$link_1 = $additional['link_1'];
        					$target_1 = $link_1['target'] ? ' target="'.$link_1['target'].'" ' : '';
        					$link_2 = $additional['link_2']; 
        					$target_2 = $link_2['target'] ? ' target="'.$link_2['target'].'" ' : '';?>
        					<div class="button__row">
        						<a class="btn yellow__btn" href="<?php echo $link_1['url']; ?>"<?php echo $target_1; ?>><?php echo $link_1['title']; ?></a>
        						<a class="btn white__btn" href="<?php echo $link_2['url']; ?>"<?php echo $target_2; ?>><?php echo $link_2['title']; ?></a>
        					</div>
        				<?php } ?>
        			</div>
        		</div>
        	</div>
        <?php }
    	} ?>
    </div>
</section>
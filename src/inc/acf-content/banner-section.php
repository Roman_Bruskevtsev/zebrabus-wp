<?php 
$background = ( get_sub_field('background') ) ? ' style="background-image: url('.get_sub_field('background').')"' : '';
?>
<div class="container">
    <div class="row">
        <div class="col">
            <div class="banner__section"<?php echo $background; ?> data-aos="fade-up">
                <?php if( get_sub_field('title') ) { ?>
                <div class="content">
                    <h2><?php the_sub_field('title'); ?></h2>
                    <?php if( get_sub_field('button_link') ){ ?>
                        <a href="<?php the_sub_field('button_link'); ?>" class="btn yellow__btn"><?php the_sub_field('button_label'); ?></a>
                    <?php } ?>
                </div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>
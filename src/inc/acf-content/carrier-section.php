<?php 
$vacancies = get_sub_field('choose_vacancies');
$args = array(
	'posts_per_page' 	=> -1,
	'orderby' 			=> 'post__in',
	'post_type'			=> 'vacancies',
	'post__in'			=> $vacancies
);
$query = new WP_Query( $args );

if( $query->have_posts() ) { ?>
<section class="carrier__section">
	<div class="container">
		<div class="row">
		<?php while ( $query->have_posts() ) { $query->the_post(); ?>
			<div class="col-md-4">
				<div class="vacancy__block">
					<h3><?php the_title(); ?></h3>
					<?php if(get_field('location')) { ?><h3><b><?php the_field('location'); ?></b></h3><?php } ?>
					<a href="<?php the_permalink(); ?>" class="btn yellow__btn"><?php _e('Read more', 'zebrabus'); ?></a>
				</div>
			</div>
		<?php } ?>
		</div>
	</div>
</section>
<?php } 
wp_reset_postdata();
?>
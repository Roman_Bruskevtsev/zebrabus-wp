<section class="partners__section">
    <div class="container">
        <?php if( get_sub_field('title') ) { ?>
        <div class="row">
            <div class="col-lg-12">
                <div class="section__title" data-aos="fade-left">
                    <h2><?php the_sub_field('title'); ?></h2>
                </div>
            </div>
        </div>
        <?php }
        if( have_rows('partners') ): ?>
        <div class="row justify-content-md-center">
            <?php while ( have_rows('partners') ) : the_row();
                $logo = get_sub_field('logo');
                $url = get_sub_field('url');
            ?>
                <div class="col-md-6 col-lg-3">
                    <?php if( $url ) { ?>
                    <a href="<?php echo $url; ?>" class="partner__block" data-aos="fade-up" target="_blank">
                    <?php } else { ?>
                    <div class="partner__block" data-aos="fade-up">
                    <?php } 
                    if( $logo ) { ?>
                        <img src="<?php echo $logo['url']; ?>" alt="<?php echo $logo['title']; ?>">
                    <?php }
                    if( $url ) { ?>
                    </a>
                    <?php } else { ?>
                    </div>
                    <?php } ?>
                </div>
            <?php endwhile; ?>
        </div>
        <?php endif; ?>
    </div>
</section>
<?php if( get_sub_field('text') ) { ?>
<div class="row">
    <div class="col">
        <div class="content__editor" data-aos="fade-up">
            <?php the_sub_field('text'); ?>
        </div>
    </div>
</div>
<?php } ?>
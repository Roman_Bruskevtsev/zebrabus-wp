<section class="routes__list">
	<div class="container">
		<?php if( get_sub_field('title') ) { ?>
        <div class="row">
            <div class="col">
                <div class="section__title" data-aos="fade-up">
                    <h2><?php the_sub_field('title'); ?></h2>
                </div>
            </div>
        </div>
        <?php } 
        $routes_list = get_sub_field('routes'); 
        if( $routes_list ) { ?>
    	<div class="row">
    		<?php foreach ( $routes_list as $route ) { ?>
    		<div class="col-lg-12">
    			<div class="route__description" data-aos="fade-up">
    				<div class="route__block">
    					<?php if( $route['main_stations'] ) { ?><h5><?php echo $route['main_stations']; ?></h5><?php } ?>
    					<?php if( $route['intermediate_stations'] ) { ?><p><?php echo $route['intermediate_stations']; ?></p><?php } ?>
    				</div>
    				<div class="dates__block">
    					<div class="float-right">
	    					<h6><?php _e('Valid for:', 'zebrabus'); ?></h6>
	    					<div class="dates"><?php echo $route['start_date']; ?> - <?php echo $route['end_date']; ?></div>
    					</div>
    				</div>
    				<div class="link__block">
					<?php if( $route['link'] ) { 
						$target = $route['link']['target'] ? ' target="'.$route['link']['target'].'"' : ''; ?>
						<a class="btn green__btn" href="<?php echo $route['link']['url'] ?>"<?php echo $target; ?>><?php echo $route['link']['title'] ?></a>
					<?php } ?>
    				</div>
    			</div>
    		</div>
    		<?php } ?>
    	</div>
        <?php } ?>
	</div>
</section>
<?php 
$background_color = get_sub_field('background_color') ? ' style="background-color:'.get_sub_field('background_color').'"' : '';
$background = get_sub_field('image') ? ' style="background-image: url('.get_sub_field('image')['url'].')"' : ''; 
$logo = get_sub_field('logo'); ?>
<section class="advertising__section">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="advertising"<?php echo $background_color; ?>>
					<div class="image"<?php echo $background; ?>></div>
					<div class="logo">
						<?php if( $logo ) { ?>
							<img src="<?php echo $logo['url']; ?>" alt="<?php echo $logo['title']?>">
						<?php } ?>
					</div>
					<?php if( get_sub_field('text') ) { ?>
					<div class="text">
						<?php the_sub_field('text'); ?>
					</div>
					<?php } 
					$link = get_sub_field('link'); 
					if( $link ) { 
						$target = $link['target'] ? ' target="'.$link['target'].'"' : '';
						?>
						<a class="btn adv__btn" href="<?php echo $link['url']; ?>"<?php echo $target; ?>><?php echo $link['title']; ?></a>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>
</section>
<?php 
$images = get_sub_field('images');
if( $images ) { ?>
<div class="row">
    <div class="col">
        <div class="gallery__images">
        <?php foreach( $images as $image ) { ?>
            <a href="<?php echo $image['url'] ?>" class="gallery" data-aos="fade-up">
                <img src="<?php echo $image['sizes']['gallery'] ?>" width="<?php echo $image['sizes']['gallery-width']; ?>" height="<?php echo $image['sizes']['gallery-height']; ?>" alt="<?php echo $image['title'] ?>">
            </a>
        <?php } ?>
        </div>
    </div>
</div>
<?php } ?>
<section class="coach scroll" id="coach">
    <div class="container">
        <?php if( get_sub_field('title') ) { ?>
        <div class="row">
            <div class="col-lg-12">
                <div class="section__title" data-aos="fade-left">
                    <h2><?php the_sub_field('title'); ?></h2>
                </div>
            </div>
        </div>
        <?php } 
        if( have_rows('buses') ): ?>
        <div class="row">
            <div class="col">
            <?php while ( have_rows('buses') ) : the_row(); 
                $image = get_sub_field('image');
            ?>
                <div class="bus__block" data-aos="fade-left">
                    <?php if($image){ ?>
                    <div class="image">
                        <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['title']; ?>">
                    </div>
                    <?php } ?>
                    <?php if( get_sub_field('title') ){ ?>
                        <h6><?php the_sub_field('title'); ?></h6>
                    <?php } ?>
                </div>
            <?php endwhile; ?>
            </div>
        </div>
        <?php endif; 
        if( get_sub_field('services_title') ) { ?>
        <div class="row">
            <div class="col-lg-12">
                <div class="section__title" data-aos="fade-left">
                    <h3 data-aos="fade-left"><?php the_sub_field('services_title'); ?></h3>
                </div>
            </div>
        </div>
        <?php } 
        if( have_rows('services') ): ?>
        <div class="row">
            <?php while ( have_rows('services') ) : the_row(); 
                $service = get_sub_field('icon');
            ?>
            <div class="col-md-6 col-lg-4">
                <div class="service" data-aos="fade-up">
                    <?php if($service){ ?>
                    <div class="icon">
                        <img src="<?php echo $service['url']; ?>" alt="<?php echo $service['title']; ?>">
                    </div>
                    <?php }
                    if( get_sub_field('title') ){ ?>
                        <h6><?php the_sub_field('title'); ?></h6>
                    <?php } ?>
                </div>
            </div>
            <?php endwhile; ?>
        </div>
        <?php endif; ?>
        <div class="row">
            <div class="col-lg-6">
                <div class="city__stick">
                    <?php if( get_sub_field('cities_title') ){ ?>
                    <h3 data-aos="fade-left"><?php the_sub_field('cities_title'); ?></h3>
                    <?php } ?>
                    <?php if( get_sub_field('cities') ){ ?>
                    <div class="table" data-aos="fade-up">
                        <?php the_sub_field('cities'); ?>
                    </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
    <div class="zebra__quotes__block" data-aos="fade-left">
        <div class="text">
            <?php if( get_sub_field('zebra_text') ){ ?>
            <h4><?php the_sub_field('zebra_text'); ?></h4>
            <?php }
            if( get_sub_field('contact_link') ){ ?>
            <a href="<?php the_sub_field('contact_link'); ?>" class="btn simple__btn contact__us"><?php the_sub_field('contact_label'); ?></a>
            <?php } ?>
        </div>
        <?php if( get_sub_field('zebra_image') ){ 
            $zebra = get_sub_field('zebra_image');
        ?>
        <div class="image">
            <img src="<?php echo $zebra['url']; ?>" alt="<?php echo $zebra['title']; ?>">
        </div>
        <?php } ?>
    </div>
</section>
<section class="how__we__work__section">
	<div class="container">
        <?php if( get_sub_field('title') ) { ?>
        <div class="row">
            <div class="col">
                <div class="section__title" data-aos="fade-up">
                    <h2><?php the_sub_field('title'); ?></h2>
                </div>
            </div>
        </div>
        <?php } 
        if( get_sub_field('text')  ) { ?>
        <div class="row">
            <div class="col-lg-8">
                <div class="text"><?php the_sub_field('text'); ?></div>
            </div>
        <?php } ?>
    </div>
</section>
<div class="container">
    <div class="row">
        <?php if( get_sub_field('email') ) { ?>
        <div class="col-sm-6 col-md-6 col-lg-3">
            <div class="contact__block" data-aos="fade-up">
                <div class="content">
                    <img src="<?php echo get_template_directory_uri().'/assets/images/contact_icon_1.svg'; ?>">
                    <a href="mailto:<?php the_sub_field('email'); ?>"><h5><?php the_sub_field('email'); ?></h5></a>
                </div>
            </div>
        </div>
        <?php } ?>
        <?php if( get_sub_field('phone') ) { ?>
        <div class="col-sm-6 col-md-6 col-lg-3">
            <div class="contact__block" data-aos="fade-up">
                <div class="content">
                    <img src="<?php echo get_template_directory_uri().'/assets/images/contact_icon_2.svg'; ?>">
                    <a href="tel:<?php the_sub_field('phone'); ?>"><h5><?php the_sub_field('phone'); ?></h5></a>
                </div>
            </div>
        </div>
        <?php } ?>
        <?php if( get_sub_field('service_working') ) { ?>
        <div class="col-sm-6 col-md-6 col-lg-3">
            <div class="contact__block" data-aos="fade-up">
                <div class="content">
                    <img src="<?php echo get_template_directory_uri().'/assets/images/contact_icon_3.svg'; ?>">
                    <h5><?php the_sub_field('service_working'); ?></h5>
                </div>
            </div>
        </div>
        <?php } ?>
        <div class="col-sm-6 col-md-6 col-lg-3">
            <div class="contact__block" data-aos="fade-up">
                <div class="content">
                <?php if( get_sub_field('instagram') ) { ?>
                    <a href="<?php the_sub_field('instagram'); ?>" target="_blank">
                        <img src="<?php echo get_template_directory_uri().'/assets/images/instagram.svg'; ?>">
                    </a>
                <?php } ?>
                <?php if( get_sub_field('facebook') ) { ?>
                    <a href="<?php the_sub_field('facebook'); ?>" target="_blank">
                        <img src="<?php echo get_template_directory_uri().'/assets/images/facebook.svg'; ?>">
                    </a>
                <?php } ?>
                <?php if( get_sub_field('social_title') ) { ?>
                    <h5><?php the_sub_field('social_title'); ?></h5>
                <?php } ?>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6">
            <div class="form__block" data-aos="fade-right">
                <?php if( get_sub_field('form_title') ) { ?>
                <h3><?php the_sub_field('form_title'); ?></h3>
                <?php } ?>
                <?php if( get_sub_field('contact_form_shortcode') ) { 
                    echo do_shortcode( get_sub_field('contact_form_shortcode') ); 
                } ?>
            </div>
            <?php if( get_sub_field('about_company') ) { ?>
            <div class="about__company" data-aos="fade-right"><?php the_sub_field('about_company'); ?></div>
            <?php } ?>
        </div> 
    </div>
</div>
<div class="zebra__block bottom" data-aos="fade-left">
    <img src="<?php echo get_template_directory_uri().'/assets/images/bus_1.png';?>" alt="<?php _e('Zebra Bus', 'zebrabus'); ?>"> 
</div>

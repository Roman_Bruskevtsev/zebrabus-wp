<?php
/**
 *
 * @package WordPress
 * @subpackage Zebrabus
 * @since 1.0
 * @version 1.0
 */
?>

<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-116912859-2"></script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-116912859-2');
    </script>
    
    <?php wp_head(); ?>
    <!-- Facebook Pixel Code -->
    <script>
    !function(f,b,e,v,n,t,s)
    {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
    n.callMethod.apply(n,arguments):n.queue.push(arguments)};
    if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
    n.queue=[];t=b.createElement(e);t.async=!0;
    t.src=v;s=b.getElementsByTagName(e)[0];
    s.parentNode.insertBefore(t,s)}(window,document,'script',
    'https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '1525516527740506'); 
    fbq('track', 'PageView');
    </script>
    <noscript>
     <img height="1" width="1" 
    src="https://www.facebook.com/tr?id=1525516527740506&ev=PageView
    &noscript=1"/>
    </noscript>
    <!-- End Facebook Pixel Code -->

    <!-- Hotjar Tracking Code for www.zebrabus.pl -->
    <script>
        (function(h,o,t,j,a,r){
            h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
            h._hjSettings={hjid:1200736,hjsv:6};
            a=o.getElementsByTagName('head')[0];
            r=o.createElement('script');r.async=1;
            r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
            a.appendChild(r);
        })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
    </script>
</head>
<body <?php body_class('load');?>>
    <div class="preloader__wrapper">
        <!-- <div class="preloader__block">
            <div class="bus"></div>
            <div class="road"></div>
            <div class="people"></div>
            <div class="sign"></div>
            <div class="percent">
                <span class="number">0</span>
                <span>%</span>
            </div>
        </div> -->

    </div>
    <header id="header" class="">
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="mobile__nav">
                        <?php if( get_field('logo', 'option') ) { ?>
                        <a class="logo float-left" href="<?php echo esc_url( home_url( '/' ) ); ?>">
                            <img src="<?php the_field('logo', 'option'); ?>" alt="<?php echo get_bloginfo('name'); ?>">
                        </a>
                        <?php } ?>
                        <?php if( get_field('book_button_link', 'option') ){ ?>
                        <a href="<?php the_field('book_button_link', 'option'); ?>" target="_blank" class="btn green__btn float-right book__btn"><?php the_field('book_button_label', 'option'); ?></a>
                        <?php } ?>
                        <div class="menu__btn">
                            <span></span>
                            <span></span>
                            <span></span>
                        </div>
                    </div>
                    <div class="desktop__nav">
                        <?php if( get_field('logo', 'option') ) { ?>
                        <a class="logo float-left" href="<?php echo esc_url( home_url( '/' ) ); ?>">
                            <img src="<?php the_field('logo', 'option'); ?>" alt="<?php echo get_bloginfo('name'); ?>">
                        </a>
                        <?php } ?>
                        <?php wp_nav_menu( array(
                            'theme_location'        => 'main',
                            'container'             => 'nav',
                            'container_class'       => 'main__nav float-left'
                        ) ); 
                        if( get_field('book_button_link', 'option') ){ ?>
                        <a href="<?php the_field('book_button_link', 'option'); ?>" target="_blank" class="btn green__btn float-right"><?php the_field('book_button_label', 'option'); ?></a>
                        <?php }

                        $wpml_lang = icl_get_languages();
                        $cur_lang_li = '';
                        $all_lang_li = '';
                        $lang_mobile = '';
                        foreach ($wpml_lang as $lang ) {
                            $flag = '';
                            if($lang['code'] == 'pl'){
                                $flag = get_template_directory_uri().'/assets/images/pl.svg';
                            } elseif($lang['code'] == 'en'){
                                $flag = get_template_directory_uri().'/assets/images/en.svg';
                            } elseif($lang['code'] == 'sk'){
                                $flag = get_template_directory_uri().'/assets/images/sk.svg';
                            }
                            if($lang['active']){
                                $cur_lang_li = '<img src="'.$flag.'" alt="'.$lang['native_name'].'" />
                                                <span>'.$lang['code'].'</span>';
                                $lang_mobile .= '<li class="text-right">
                                                    <a href="'.$lang['url'].'" class="active">
                                                        <img src="'.$flag.'" alt="'.$lang['native_name'].'" />
                                                        <span>'.$lang['code'].'</span>
                                                    </a>
                                                </li>';
                            } else {
                                $all_lang_li .= '<li>
                                                    <a href="'.$lang['url'].'" class="wpml_'.$lang['code'].'">
                                                        <img src="'.$flag.'" alt="'.$lang['native_name'].'" />
                                                        <span>'.$lang['code'].'</span>
                                                    </a>
                                                </li>';
                                $lang_mobile .= '<li class="text-left">
                                                    <a href="'.$lang['url'].'">
                                                        <img src="'.$flag.'" alt="'.$lang['native_name'].'" />
                                                        <span>'.$lang['code'].'</span>
                                                    </a>
                                                </li>';
                            }
                        } ?>
                        <div class="lang__switcher float-right">
                            <div class="current">
                                <?php echo $cur_lang_li; ?>
                            </div>
                            <ul>
                                <?php echo $all_lang_li; ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <div class="mobile__menu">
        <?php wp_nav_menu( array(
            'theme_location'        => 'main',
            'container'             => 'nav',
            'container_class'       => 'main__nav'
        ) ); 
        ?>
        <div class="lang__switcher">
            <ul>
                <?php echo $lang_mobile; ?>
            </ul>
        </div>
        
    </div>

    <main id="parallax-scene">
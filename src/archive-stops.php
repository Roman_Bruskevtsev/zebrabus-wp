<?php
/**
 *
 * @package WordPress
 * @subpackage Zebrabus
 * @since 1.0
 * @version 1.0
 */
get_header(); ?>
<div class="container">
    <div class="row">
        <div class="col">
            <div class="page__title" data-aos="fade-left">
                <h1><?php _e('Stops', 'zebrabus'); ?></h1>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <?php if ( have_posts() ) : ?>
    <div class="row">
        <div class="col-md-4 col-lg-4 col-xl-4">
            <div class="map__sidebar d-block d-md-none" data-aos="fade-up">
                <ul class="stops">
                <?php while ( have_posts() ) : the_post(); ?>
                    <?php if( get_field('latitude') && get_field('longitude') ) { ?>
                    <li 
                    data-id="<?php the_ID(); ?>"
                    data-lat="<?php the_field('latitude'); ?>"
                    data-lng="<?php the_field('longitude'); ?>"
                    data-text="<?php the_field('description'); ?>"
                    data-book="<?php the_field('book_link'); ?>"
                    data-book-label="<?php the_field('book_label'); ?>"
                    data-dep="<?php the_field('departures_link'); ?>"
                    data-dep-label="<?php the_field('departures_label'); ?>"
                    data-google="<?php the_field('google_maps'); ?>"
                    data-google-label="<?php the_field('google_maps_label'); ?>" class="mobile">
                        <span class="title"><?php the_title(); ?></span>
                        <span class="link"><?php _e( 'Show on map', 'zebrabus' ); ?></span>
                    </li>
                    <?php } ?>
                <?php endwhile; ?>
                </ul>
            </div>
            <div class="map__sidebar d-none d-md-block" data-aos="fade-up">
                <ul class="stops">
                <?php while ( have_posts() ) : the_post(); ?>
                    <?php if( get_field('latitude') && get_field('longitude') ) { ?>
                    <li 
                    data-id="<?php the_ID(); ?>"
                    data-lat="<?php the_field('latitude'); ?>"
                    data-lng="<?php the_field('longitude'); ?>"
                    data-text="<?php the_field('description'); ?>"
                    data-book="<?php the_field('book_link'); ?>"
                    data-book-label="<?php the_field('book_label'); ?>"
                    data-dep="<?php the_field('departures_link'); ?>"
                    data-dep-label="<?php the_field('departures_label'); ?>"
                    data-google="<?php the_field('google_maps'); ?>"
                    data-google-label="<?php the_field('google_maps_label'); ?>" class="mobile">
                        <span class="title"><?php the_title(); ?></span>
                        <span class="link"><?php _e( 'Show on map', 'zebrabus' ); ?></span>
                    </li>
                    <?php } ?>
                <?php endwhile; ?>
                </ul>
            </div>
        </div>
    </div>
    <?php endif; ?>
</div>
<?php
$map = get_field('map', 'option');
if( $map ) { ?>
<div class="container-fluid map__section">
    <div class="row">
        <div class="offset-md-5 col-md-7 col-lg-7">
            <div class="map__container" data-lat="<?php echo $map['latitude']; ?>" data-lng="<?php echo $map['longitude']; ?>" data-zoom="<?php echo $map['zoom']; ?>" data-marker="<?php echo $map['marker']; ?>">
                <div id="google__map" class="map__wrapper"></div>
            </div>
        </div>
    </div>
</div>
<?php } ?>
<?php get_footer();
<?php
/**
 *
 * @package WordPress
 * @subpackage Zebrabus
 * @since 1.0
 * @version 1.0
 */
get_header(); 
$banner = get_field('banner');
$information = get_field('information');
if( $banner ) { 
    $background = ($banner['image']) ? ' style="background-image: url('.$banner['image'].');"' : '';
    ?>
<div class="page__banner"<?php echo $background; ?>>
    <div class="circle__container">
        <div class="circle__2 circle" data-depth="0.2"></div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col">
                <div class="content__wrapper">
                    <div class="content">
                        <?php if( $banner['title'] ) { ?><h1 data-aos="fade-left"><?php echo $banner['title']; ?></h1><?php } ?>
                        <?php if( $banner['short_description'] ) { ?>
                        <div class="description" data-aos="fade-up">
                            <?php echo $banner['short_description']; ?>
                        </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php }
if( have_rows('content') ):
    while ( have_rows('content') ) : the_row();
        if( get_row_layout() == 'information_section' ): ?>
        <div class="information">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="images__block" data-aos="fade-up">
                            <?php if( get_sub_field('image_1') ) { 
                                ?>
                                <img src="<?php echo get_sub_field('image_1')['url']; ?>" alt="<?php echo get_sub_field('image_1')['title']; ?>" class="image__1">
                            <?php }
                            if( get_sub_field('image_2') ) { 
                                ?>
                                <img src="<?php echo get_sub_field('image_2')['url']; ?>" alt="<?php echo get_sub_field('image_2')['title']; ?>" class="image__2">
                            <?php } ?>
                        </div>
                    </div>
                    <div class="col-lg-1"></div>
                    <div class="col-lg-5">
                        <?php if( get_sub_field('details') ) { ?>
                            <div class="description" data-aos="fade-up">
                                <?php echo get_sub_field('details'); ?>
                            </div>
                        <?php } 
                        if( get_sub_field('extras') ) { ?>
                            <div class="extras">
                                <?php foreach( get_sub_field('extras') as $extra ) { ?>
                                    <div class="extra" data-aos="fade-up">
                                        <?php if( $extra['icon'] ) { ?>
                                            <img src="<?php echo $extra['icon']['url']; ?>" alt="<?php echo $extra['title']; ?>">
                                        <?php } ?>
                                        <?php if( $extra['title'] ) { ?>
                                            <h5><?php echo $extra['title']; ?></h5>
                                        <?php } ?>
                                    </div>
                                <?php } ?>
                            </div>
                        <?php } ?>
                        <div class="btn__row">
                            <?php if( get_sub_field('book_link') ) { ?>
                            <a href="<?php echo get_sub_field('book_link'); ?>" class="btn green__btn shadow float-left" data-aos="fade-up"><?php echo get_sub_field('book_label'); ?></a>
                            <?php } ?>
                            <?php if( get_sub_field('timetable_label') ) { ?>
                            <a href="#timetable" class="btn simple__btn shadow float-left" data-aos="fade-up"><?php echo get_sub_field('timetable_label'); ?></a>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php endif; 
    endwhile; ?>
    <div class="additional__information gradient" id="timetable">
        <div class="circle__container">
            <div class="circle__3 circle" data-depth="0.2"></div>
            <div class="circle__4 circle" data-depth="0.3"></div>
        </div>
        <div class="zebra__block" data-aos="fade-left">
            <img src="<?php echo get_template_directory_uri().'/assets/images/bus_2.png';?>" alt="<?php _e('Zebra Bus', 'zebrabus'); ?>"> 
        </div>
        <div class="container">
        <?php 
        while ( have_rows('content') ) : the_row();
            if( get_row_layout() == 'timetable_section' ) :
                if( get_sub_field('title') ) { ?>
                <div class="row">
                    <div class="col">
                        <h2 data-aos="fade-left"><?php echo get_sub_field('title'); ?></h2>
                    </div>
                </div>
                <?php } 
                $routes_list = get_sub_field('routes_list'); 
                if( $routes_list ) { ?>
                <div class="row">
                    <?php foreach ( $routes_list as $route ) { ?>
                    <div class="col-lg-12">
                        <div class="route__description" data-aos="fade-up">
                            <div class="route__block">
                                <?php if( $route['main_stations'] ) { ?><h5><?php echo $route['main_stations']; ?></h5><?php } ?>
                                <?php if( $route['intermediate_stations'] ) { ?><p><?php echo $route['intermediate_stations']; ?></p><?php } ?>
                            </div>
                            <div class="dates__block">
                                <div class="float-right">
                                    <h6><?php _e('Valid for:', 'zebrabus'); ?></h6>
                                    <div class="dates"><?php echo $route['start_date']; ?> - <?php echo $route['end_date']; ?></div>
                                </div>
                            </div>
                            <div class="link__block">
                            <?php if( $route['link'] ) { 
                                $target = $route['link']['target'] ? ' target="'.$route['link']['target'].'"' : ''; ?>
                                <a class="btn green__btn" href="<?php echo $route['link']['url'] ?>"<?php echo $target; ?>><?php echo $route['link']['title'] ?></a>
                            <?php } ?>
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                </div>
                <?php } ?>
            <?php elseif ( get_row_layout() == 'special_offer_section' ) : ?>
                <div class="special__offer">
                    <?php if( get_sub_field('title') ) { ?>
                    <div class="row">
                        <div class="col">
                            <h3 class="special" data-aos="fade-left"><?php echo get_sub_field('title'); ?></h3>
                        </div>
                    </div>
                    <?php } ?>
                    <div class="row justify-content-md-center">
                        <div class="col-md-12 col-lg-10 col-xl-12">
                            <div class="special__block" data-aos="fade-up">
                                <?php if( get_sub_field('image') ) { ?>
                                    <img src="<?php echo get_sub_field('image')['url']; ?>" alt="<?php echo get_sub_field('image')['title']; ?>" class="d-none d-lg-block">
                                <?php } ?>
                                <div class="special__description">
                                    <?php if( get_sub_field('subtitle') ) { ?>
                                        <h5 class="bold"><?php echo get_sub_field('subtitle'); ?></h5>
                                    <?php } ?>
                                    <?php if( get_sub_field('text') ) { ?>
                                        <p><?php echo get_sub_field('text'); ?></p>
                                    <?php } ?>
                                    <?php if( get_sub_field('prices') ) { ?>
                                    <div class="price__row">
                                        <?php foreach ( get_sub_field('prices') as $price) { ?>
                                        <div class="price">
                                            <?php if( $price['icon'] ) { ?>
                                            <div class="icon">
                                                <img src="<?php echo $price['icon']['url']; ?>" alt="<?php echo $price['icon']['title']; ?>">
                                            </div>
                                            <?php } ?>
                                            <div class="text">
                                                <?php if( $price['title'] ) { ?><h6><?php echo $price['title']; ?></h6><?php } ?>
                                                <?php if( $price['price_with_discount'] ) { ?><span class="new__price"><?php echo $price['price_with_discount']; ?></span><?php } ?>
                                                <?php if( $price['price'] ) { ?><span class="old__price"><?php echo $price['price']; ?></span><?php } ?>
                                            </div>
                                        </div>
                                        <?php } ?>
                                    </div>
                                    <?php } ?>
                                    <?php if( get_sub_field('buy_ticket_link') ) { ?>
                                        <a href="<?php echo get_sub_field('buy_ticket_link'); ?>" class="btn green__btn shadow"><?php echo get_sub_field('buy_ticket_label'); ?></a>
                                    <?php } ?>
                                    <?php if( get_sub_field('timetable_link') ) { ?>
                                        <a href="<?php echo get_sub_field('timetable_link'); ?>" target="_blank" class="btn simple__btn shadow"><?php echo get_sub_field('timetable_label'); ?></a>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php elseif ( get_row_layout() == 'tickets_section' ) : ?>
                <div class="tickets">
                    <?php if( get_sub_field('title') ) { ?>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="section__title" data-aos="fade-left">
                                <h2><?php echo get_sub_field('title'); ?></h2>
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                    <div class="row">
                        <div class="col-lg-6">
                            <?php 
                            $pricelist = get_sub_field('pricelist');
                            if( $pricelist['title'] ) { ?>
                            <div class="table__title" data-aos="fade-left">
                                <img src="<?php echo get_template_directory_uri().'/assets/images/table_icon_1.svg'; ?>" alt="<?php echo $pricelist['title']; ?>">
                                <h3><?php echo $pricelist['title']; ?></h3>
                            </div>
                            <?php }
                            if( $pricelist['directions'] ) { ?>
                            <div class="pricelist__table" data-aos="fade-up">
                                <?php foreach ( $pricelist['directions'] as $direction ) { ?>
                                <div class="table__row">
                                    <?php if( $direction['title'] ) { ?>
                                    <div class="stations"><?php echo $direction['title']; ?></div>
                                    <?php } ?>
                                    <?php if( $direction['adult_price'] ) { ?>
                                    <div class="full__price">
                                        <div class="price">
                                            <img src="<?php echo get_template_directory_uri().'/assets/images/adult_ticket.svg'; ?>" alt="<?php _e('Adult', 'zebrabus'); ?>">
                                            <span><?php echo $direction['adult_price']; ?></span>
                                        </div>
                                    </div>
                                    <?php } ?>
                                    <?php if( $direction['child_price'] ) { ?>
                                    <div class="child__price">
                                        <div class="price float-right">
                                            <img src="<?php echo get_template_directory_uri().'/assets/images/child_ticket.svg'; ?>" alt="<?php _e('Child', 'zebrabus'); ?>">
                                            <span><?php echo $direction['child_price']; ?></span>
                                        </div>
                                    </div>
                                    <?php } ?>
                                </div>
                                <?php } 
                                if( $pricelist['promo_title'] || $pricelist['promo_price'] ) { ?>
                                <div class="table__row">
                                    <?php if( $pricelist['promo_title'] ) { ?><div class="promo__title"><?php echo $pricelist['promo_title']; ?></div><?php } ?>
                                    <?php if( $pricelist['promo_price'] ) { ?><div class="promo__price"><?php echo $pricelist['promo_price']; ?></div><?php } ?>
                                </div>
                            <?php } 
                            if( $pricelist['link'] ) { ?>
                                <div class="table__row text-center">
                                    <?php $target = $pricelist['link']['target'] ? ' target="'.$pricelist['link']['target'].'"' : ''; ?>
                                    <a class="btn black__btn" href="<?php echo $pricelist['link']['url'] ?>"<?php echo $target; ?>><?php echo $pricelist['link']['title'] ?></a>
                                </div>
                            <?php } ?>
                            </div>
                        <?php } ?>
                        </div>
                        <div class="col-lg-6">
                            <?php $discounts = get_sub_field('discounts');
                            if( $discounts['title'] ) { ?>
                            <div class="table__title" data-aos="fade-left">
                                <img src="<?php echo get_template_directory_uri().'/assets/images/table_icon_3.svg'; ?>" alt="<?php echo $discounts['title']; ?>">
                                <h3><?php echo $discounts['title']; ?></h3>
                            </div>
                            <?php } ?>
                            <?php foreach ($discounts['discount'] as $discount) { ?>
                            <div class="discount__table" data-aos="fade-up">
                                <div class="table__row">
                                    <?php if( $discount['icon']) { ?>
                                    <img src="<?php echo $discount['icon']['url']; ?>" alt="<?php echo $discount['icon']['title']; ?>">
                                    <?php } ?>
                                    <div class="text">
                                        <?php if( $discount['title']) { ?>
                                        <h6><?php echo $discount['title']; ?></h6>
                                        <?php } ?>
                                        <?php if( $discount['description']) { ?>
                                        <p>
                                            <?php echo $discount['description']; ?>
                                        </p>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                            <?php } ?>
                        </div>
                    </div>
                    <?php 
                    $busline = get_sub_field('busline_price');
                    if( $busline ) { ?>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="busline__block">
                                <?php if( $busline['title'] ) { ?>
                                    <h3 data-aos="fade-left"><?php echo $busline['title']; ?></h3>
                                <?php } 
                                $buslines = $busline['buslines']; 
                                if( $buslines ) { 
                                    foreach ($buslines as $line) { ?>
                                        <div class="busline__row" data-aos="fade-up">
                                            <div class="icon"></div>
                                            <div class="stations"><h5><?php echo $line['stations']; ?></h5></div>
                                            <div class="link text-right">
                                            <?php if( $line['link'] ) { 
                                                $target = $line['link']['target'] ? ' target="'.$line['link']['target'].'"' : ''; ?>
                                                <a class="btn black__btn" href="<?php echo $line['link']['url'] ?>"<?php echo $target; ?>><?php echo $line['link']['title'] ?></a>
                                            <?php } ?>
                                            </div>
                                        </div>
                                    <?php }
                                } ?>
                            </div>
                        </div>
                    </div>
                    <?php } 
                    $howto = get_sub_field('how_to_buy');
                    if( $howto['title'] ) { ?>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="table__title" data-aos="fade-left">
                                <img src="<?php echo get_template_directory_uri().'/assets/images/table_icon_2.svg'; ?>" alt="<?php echo $howto['title']; ?>">
                                <h3><?php echo $howto['title']; ?></h3>
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                    <div class="row">
                        
                        <div class="col-sm-6 col-lg-3">
                            <div class="buy__block online" data-aos="fade-up">
                            <?php if( $howto['online_title'] ) { ?>
                                <h5><?php echo $howto['online_title']; ?></h5>
                            <?php } ?>
                            <?php if( $howto['online_icon'] ) { ?>
                                <img src="<?php echo $howto['online_icon']['url']; ?>" alt="<?php echo $howto['online_icon']['title']; ?>">
                            <?php } ?>
                            <?php if( $howto['online_link'] ) { ?>
                                <a href="<?php echo $howto['online_link']; ?>" target="_blank" class="btn simple__btn"><?php echo $howto['online_label']; ?></a>
                            <?php } ?>
                            </div>
                        </div>
                        
                        <div class="col-sm-6 col-lg-3">
                            <div class="buy__block" data-aos="fade-up">
                                <?php if( $howto['office_title'] ) { ?>
                                    <h5><?php echo $howto['office_title']; ?></h5>
                                <?php } ?>
                                <?php if( $howto['office_icon'] ) { ?>
                                    <img src="<?php echo $howto['office_icon']['url']; ?>" alt="<?php echo $howto['office_icon']['title']; ?>">
                                <?php } ?>
                                <?php if( $howto['office_link'] ) { ?>
                                    <a href="<?php echo $howto['office_link']; ?>" target="_blank" class="btn simple__btn"><?php echo $howto['office_label']; ?></a>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="buy__block large" data-aos="fade-up">
                                <div class="content">
                                    <?php if( $howto['driver_icon'] ) { ?>
                                        <img src="<?php echo $howto['driver_icon']['url']; ?>" alt="<?php echo $howto['driver_icon']['title']; ?>">
                                    <?php } ?>
                                    <?php if( $howto['driver_title'] ) { ?>
                                        <h5><?php echo $howto['driver_title']; ?></h5>
                                    <?php } ?>
                                    
                                    <?php if( $howto['driver_text'] ) { ?>
                                        <p><?php echo $howto['driver_text']; ?></p>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php 
                    $advertising = get_sub_field('advertising_block'); 
                    if( $advertising ) { 
                        $background = $advertising['image'] ? ' style="background-image: url('.$advertising['image']['url'].')"' : ''; 
                        $logo = $advertising['logo']; ?>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="advertising" data-aos="fade-up">
                                <div class="image"<?php echo $background; ?>></div>
                                <div class="logo">
                                    <?php if( $logo ) { ?>
                                        <img src="<?php echo $logo['url']; ?>" alt="<?php echo $logo['title']?>">
                                    <?php } ?>
                                </div>
                                <?php if( $advertising['text'] ) { ?>
                                <div class="text">
                                    <?php echo $advertising['text']; ?>
                                </div>
                                <?php } 
                                $link = $advertising['link']; 
                                if( $link ) { 
                                    $target = $link['target'] ? ' target="'.$link['target'].'"' : '';
                                    ?>
                                    <a class="btn adv__btn" href="<?php echo $link['url']; ?>"<?php echo $target; ?>><?php echo $link['title']; ?></a>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                </div>
            <?php endif; ?>
        <?php endwhile; ?>
        </div>
    </div>
    <?php while ( have_rows('content') ) : the_row(); 
        if( get_row_layout() == 'buy_a_ticket_section' ) : ?>
            <div class="posts__section margin">
                <div class="container">
                    <?php if( get_sub_field('title') ) { ?>
                    <div class="row">
                        <div class="col">
                            <h2 data-aos="fade-up"><?php echo get_sub_field('title'); ?></h2>
                        </div>
                    </div>
                    <?php }
                    if( get_sub_field('operate_blocks') ){
                        $posts = get_sub_field('operate_blocks');
                        if ( $posts ) { ?>
                            <div class="row">
                            <?php foreach ($posts as $post) {
                                get_template_part( 'template-parts/routes/content-simple');
                            } ?>
                            </div>
                        <?php } ?>
                    <?php } ?>
                </div>
            </div>
        <?php endif;
    endwhile; ?>
<?php endif;
get_footer();